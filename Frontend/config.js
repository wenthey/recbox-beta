/*jslint stupid: true */

var extend      = require('extend');
var fs          = require('fs');
var LocalConfig = {};

// any local changes should be made in ./config.local.js
if (fs.existsSync('./config.local.js')) {
    LocalConfig = require('./config.local.js');
}

module.exports = extend(true, {
    name:                  'audiobox-dev',          // audiobox name used for server identification
    audioLibraryFolder:    './resources',          // audio library path (mp3/wav, png/jpg, lrc); set in config.local.js
    lyricsLibraryFolder:    './public/resources',   //
    imagesFullFolder:       './resources',          //
    imagesThumbnailsFolder: './resources',          // audio library path (mp3/wav, png/jpg, lrc); set in config.local.js
    audioRecordingsFolder: './public/recordings',   // folder for output recordings; set this in config.local.js!
    fullscreen:            false,                   // if should be run in fullscreen
    startView:             'slideshow',             // initial view for state-machine
    serverUrl:             'http://localhost:3001', // url of server application
    screenRatioFactor:     0.5,                     // scaling factor for default screen size (1920 x 1080)
    devToolsWidth:         350,                     // width for devtools (only if isProduction === false) (px)
    serverPingInterval:    1000,                    // interval for socket.io pings to server (ms)
    transitionTime:        3000,                    // artificial delay between views (ms)
    endScreenTimeout:      10000,                   // timeout before restarting on endscreen
    backendMock:           false,                   // Mock backend for development
    backendSendPort:       3003,                    // OSC port to send data to backend on localhost
    backendReceivePort:    3004                     // OSC port for receiving data from backend on localhost
}, LocalConfig);

