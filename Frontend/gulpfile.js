var chalk     = require('chalk');
var gulp      = require('gulp');
var jscs      = require('gulp-jscs');
var jslint    = require('gulp-jslint');
var log       = require('gulp-util').log;
var spawn     = require('child_process').spawn;
var watch     = require('gulp-watch');

var lintFiles = [ '*.js', 'src/*.js', 'src/**/*.js' ];

var error = function(error) {
  log(chalk.red(error.message));
  this.emit('end');
};

var shell = function(command, name) {
  var cmd  = command.split(' ').shift();
  var args = command.split(' ').slice(1);

  name = name || cmd;

  var spawned = spawn(cmd, args);

  spawned.stdout.on('data', function(data) {
    data
      .toString()
      .split('\n')
      .map(function(line)     { return line.replace(/^\.*\]/, '').trim(); })
      .filter(function(line)  { return line.length > 1; })
      .forEach(function(line) { log(chalk.cyan(name) + ' ' + line); });
  });

  spawned.on('exit', function() {
    this.emit('done');
  });

  return spawned;
};

var lint = function() {
  return gulp.src(lintFiles)
    .pipe(jslint({
      errorsOnly: true,
      newcap:     true,
      node:       true,
      nomen:      true,
      regexp:     true,
      sloppy:     true,
      todo:       true,
      vars:       true,
      white:      true
    }).on('error', error))
    .pipe(jscs().on('error', error));
};

gulp.task('watch', function() {
  watch(lintFiles, function() {
    gulp.start('lint');
    // gulp.start('test');
  });
});

gulp.task('electron', function() {
  return shell('./node_modules/.bin/electron main.js', 'electron');
});

gulp.task('test', function() {
  return shell('./node_modules/.bin/tape test/*', 'test');
});

gulp.task('lint', function() { return lint(); });

gulp.task('default', [ 'electron', 'lint', 'watch' ]);
