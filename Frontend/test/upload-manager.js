var upload = require('../src/upload-manager');

console.log('warning! upload manager deletes the file after upload!');

upload.uploadFile(
  '/Users/Szymon/Desktop/test.mp3',
  {
    artist: 'ORCAS',
    title:  'Until Then',
    email:  'kaliskiszymon@gmail.com'
  },
  function() {
    console.log('upload done!');
    process.exit(0);
  }
);
