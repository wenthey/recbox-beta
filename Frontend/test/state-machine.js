var test = require('tape');
var StateMachine = require('../src/state-machine');

var errorOccured = function(err) {
  return (err === undefined || err === null) ? false : true;
};

test('initializing-with-empty', function(t) {
  var testObject = {
    initialState: null,
    states: null
  };

  var machine;

  try {
    machine = new StateMachine(testObject);
  }
  catch (err) {
    t.equal(errorOccured(err), true);
  }

  t.end();
});

test('no-initial-state', function(t) {
  var testObject = {
    initialState: null,
    states: [
      {
        name: 'login',
        transitions: {
          next: 'choose'
        }
      }
    ]
  };

  var machine;

  try {
    machine = new StateMachine(testObject);
  }
  catch (err) {
    t.equal(errorOccured(err), true);
  }

  t.end();
});

test('invalid-initial-state', function(t) {
  var testObject = {
    initialState: 'xxx',
    states: [
      {
        name: 'login',
        transitions: {
          next: 'choose'
        }
      },
      {
        name: 'choose',
        transitions: {
          next: 'instructinos'
        }
      }
    ]
  };

  var machine;

  try {
    machine = new StateMachine(testObject);
  }
  catch (err) {
    t.equal(errorOccured(err), true);
  }

  t.end();
});

test('invalid-transition', function(t) {
  var testObject = {
    initialState: 'login',
    states: [
      {
        name: 'login',
        transitions: {
          next: 'choose'
        }
      },
      {
        name: 'choose',
        transitions: {
          next: 'instructinos'
        }
      },
      {
        name: 'instructinos',
        transitions: {
          next: 'karaoke'
        }
      },
      {
        name: 'karaoke',
        transitions: {
          next: 'byebye'
        }
      }
    ]
  };

  var machine = new StateMachine(testObject);

  try {
    machine.transition('back');
  }
  catch (err) {
    t.equal(errorOccured(err), true);
  }

  t.end();
});

test('one-valid-transition', function(t) {
  var testObject = {
    initialState: 'login',
    states: [
      {
        name: 'login',
        transitions: {
          next: 'choose'
        }
      }
    ]
  };

  var machine = new StateMachine(testObject);
  var result = machine.transition('next');

  t.equal(errorOccured(result), false);


  t.end();
});

test('few-valid-transitions', function(t) {
  var testObject = {
    initialState: 'login',
    states: [
      {
        name: 'login',
        transitions: {
          next: 'choose'
        }
      },
      {
        name: 'choose',
        transitions: {
          next: 'instructinos'
        }
      },
      {
        name: 'instructinos',
        transitions: {
          next: 'karaoke'
        }
      },
      {
        name: 'karaoke',
        transitions: {
          next: 'byebye'
        }
      }
    ]
  };

  var machine = new StateMachine(testObject);

  var result = machine.transition('next');
  var result1 = machine.transition('next');
  var result2 = machine.transition('next');
  var result3 = machine.transition('next');

  t.equal(errorOccured(result), false);
  t.equal(errorOccured(result1), false);
  t.equal(errorOccured(result2), false);
  t.equal(errorOccured(result3), false);

  t.end();
});


test('initial-state-in-the-middle', function(t) {
  var testObject = {
    initialState: 'instructinos',
    states: [
      {
        name: 'login',
        transitions: {
          next: 'choose'
        }
      },
      {
        name: 'choose',
        transitions: {
          next: 'instructinos'
        }
      },
      {
        name: 'instructinos',
        transitions: {
          next: 'karaoke'
        }
      },
      {
        name: 'karaoke',
        transitions: {
          next: 'byebye'
        }
      }
    ]
  };

  var machine = new StateMachine(testObject);

  var result = machine.transition('next');
  var result1 = machine.transition('next');

  t.equal(errorOccured(result), false);
  t.equal(errorOccured(result1), false);

  t.end();
});

test('more-transitions-than-states', function(t) {
  var testObject = {
    initialState: 'login',
    states: [
      {
        name: 'login',
        transitions: {
          next: 'choose'
        }
      },
      {
        name: 'choose',
        transitions: {
          next: 'instructinos'
        }
      },
      {
        name: 'instructinos',
        transitions: {
          next: 'karaoke'
        }
      },
      {
        name: 'karaoke',
        transitions: {
          next: 'byebye'
        }
      }
    ]
  };

  var machine = new StateMachine(testObject);

  var result = machine.transition('next');
  var result1 = machine.transition('next');
  var result2 = machine.transition('next');
  var result3 = machine.transition('next');
  var result4 = null;

  try {
    machine.transition('next');
  }
  catch (err) {
    console.log(err);
    result4 = err;
  }

  t.equal(errorOccured(result), false);
  t.equal(errorOccured(result1), false);
  t.equal(errorOccured(result2), false);
  t.equal(errorOccured(result3), false);
  t.equal(errorOccured(result4), true);

  t.end();
});

test('few-valid-transitions-defined-not-in-order', function(t) {
  var testObject = {
    initialState: 'login',
    states: [
      {
        name: 'choose-song',
        transitions: {
          next: 'mic-test'
        }
      },
      {
        name: 'karaoke',
        transitions: {
          next: 'end-screen'
        }
      },
      {
        name: 'mic-test',
        transitions: {
          next: 'karaoke'
        }
      },
      {
        name: 'login',
        transitions: {
          next: 'choose-song'
        }
      }
    ]
  };

  var machine = new StateMachine(testObject);

  var result = machine.transition('next');
  var result1 = machine.transition('next');
  var result2 = machine.transition('next');
  var result3 = machine.transition('next');

  t.equal(errorOccured(result), false);
  t.equal(errorOccured(result1), false);
  t.equal(errorOccured(result2), false);
  t.equal(errorOccured(result3), false);

  t.end();
});
