var AudioLibrary = require('../src/audio-library');
var audioLibrary = new AudioLibrary();

audioLibrary.generate(function() {
  console.log('generated!');

  audioLibrary.find({ text: 'varius' }, function(error, results) {
    console.log('\nitems found for text \'various\'');
    console.log(results);
  });

  audioLibrary.find({ genre: 'pop' }, function(error, results) {
    console.log('\nitems found for genre \'pop\'');
    console.log(results);
  });

  audioLibrary.listGenres(function(error, genres) {
    console.log('\ngenres in db');
    console.log(genres);
  });

  audioLibrary.getNewest(20, function(error, newest) {
    console.log('\n20 newest files');
    console.log(newest);
  });
});

