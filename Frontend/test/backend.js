var Backend            = require('../src/backend');
var audioLibraryFolder = require('../config').audioLibraryFolder;
var audioRecordingsFolder = require('../config').audioRecordingsFolder;
var log                = require('gulp-util').log;

var backend = new Backend();

backend.on('progress', function(e) {
  log('karaoke progress', e);
});

backend.on('record:start', function() {
  log('record started!');
});

backend.on('record:done', function() {
  log('record done!');
});

backend.on('microphone', function(e) {
  log('microphone vol', e);
});

backend.on('mictest:done', function() {
  log('mictest done!');
});

// setTimeout(function() {
//   console.log('starting mictest...');
//   backend.startMicTest();
// }, 1000);

// setTimeout(function() {
//   console.log('setting volume to 0.8');
//   backend.setHeadphonesVolume(0.8);

//   setTimeout(function() {
//     console.log('setting volume to 0.3');
//     backend.setHeadphonesVolume(0.3);
//   }, 1000);
// }, 1000);

setTimeout(function() {
  console.log('loading song...');
  backend.loadSong(audioLibraryFolder + '/1.mp3');
}, 1000);

setTimeout(function() {
  console.log('recording song...');
  backend.recordSong(audioRecordingsFolder + 'out.wav');
}, 1000);

setTimeout(function() {
  // wait for data
}, 10000000);
