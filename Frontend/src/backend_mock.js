var Config       = require('../config');
var EventEmitter = require('events').EventEmitter;
var UdpReceiver  = require('omgosc').UdpReceiver;
var UdpSender    = require('omgosc').UdpSender;
var util         = require('util');

function Backend() {
  this.udpSender   = new UdpSender('127.0.0.1', Config.backendSendPort);
  this.udpReceiver = new UdpReceiver(Config.backendReceivePort);
  this.mockMictestStart = false;
  this.mockRecordStart = false;
  this.mockRecordStart_defaultDuration = 229;
  this.mockRecordStart_currentDuration = 0;

  setInterval(function(){

    //// emits microphone volume during mic-test
    if (true === this.mockMictestStart) {
      var min = -60,
          max = 10,
          maxTime = 1000,
          refreshInterval = 100,
          refresh = refreshInterval;

      while (refresh <= maxTime) {
        setTimeout(function(){ this.emit('microphone', { volume: Math.floor(Math.random() * (max - min + 1) + min) }) }.bind(this, min, max), refresh += refreshInterval); 
      }
    }
  }.bind(this), 1000);

    setInterval(function(){
      // emits song progress on karaoke
      if (true === this.mockRecordStart) {
          this.mockRecordStart_currentDuration = this.mockRecordStart_currentDuration + 0.05;

          if (this.mockRecordStart_currentDuration <= this.mockRecordStart_defaultDuration) {

              this.emit('progress', {progress: (this.mockRecordStart_currentDuration / this.mockRecordStart_defaultDuration)} );

          } else {
              // ### 3. normal
              this.emit('progress', {progress: 1} );

              setTimeout(function(){ this.emit('record:done'); }.bind(this), 1500);

              this.mockRecordStart = false;
          }
      } else {
          this.mockRecordStart_currentDuration = 0;
      }
    }.bind(this), 50);

  //this.udpReceiver.on('/progress', function(e) {
  //  this.emit('progress', { progress: e.params[0] });
  //}.bind(this));
  //
  //// emits when recording is done (and file is saved to disk for upload!)
  //this.udpReceiver.on('/record-done', function() {
  //  this.emit('record:done');
  //}.bind(this));

}

util.inherits(Backend, EventEmitter);

/**
 * set headphones volume
 * @param {number} vol - desired volume [0..1]
 */
Backend.prototype.setHeadphonesVolume = function(vol) {
  this.udpSender.send('/volume', 'f', [ vol ]);
};

/**
 * loads song background in backend
 * requires at least 1000ms before recordSong can start
 * @param {string} path - full song path ("/Users/Audiobox/Songs/artist-title.mp3")
 */
Backend.prototype.loadSong = function(path) {
  this.udpSender.send('/load-song', 's', [ path ]);
};

/**
 * start recording singing into the microphone
 * will start sending /progress messages
 * @param {string} path - full path for output file ("/Users/Audiobox/Output/_id_.wav")
 */
Backend.prototype.recordSong = function(path) {
  console.log('/record-start');
  this.mockRecordStart = true;
};

/**
 * start automatic mic calibration
 * will start sending /microphone messages with volume
 * will send mictest-done (mictest:done) on end
 */
Backend.prototype.startMicTest = function() {
  this.mockMictestStart = true;
    console.log('mictest:start');

    //// emits when mic test is done (after .startMicTest())
  setTimeout(function(){
    this.mockMictestStart = false;
    this.emit('mictest:done');
    console.log('mictest:done');
  }.bind(this), 2000);
};

Backend.prototype.playRecording = function() {
  this.udpSender.send('/replay-start', 'T', [ true ]);
};

Backend.prototype.pauseRecording = function() {
  this.udpSender.send('/replay-stop', 'T', [ true ]);
};

module.exports = Backend;
