var Config       = require('../config');
var EventEmitter = require('events').EventEmitter;
var UdpReceiver  = require('omgosc').UdpReceiver;
var UdpSender    = require('omgosc').UdpSender;
var util         = require('util');

function Backend() {
  this.udpSender   = new UdpSender('127.0.0.1', Config.backendSendPort);
  this.udpReceiver = new UdpReceiver(Config.backendReceivePort);

  // emits song progress on karaoke
  this.udpReceiver.on('/progress', function(e) {
    this.emit('progress', { progress: e.params[0] });
  }.bind(this));

  // emits when recording is done (and file is saved to disk for upload!)
  this.udpReceiver.on('/record-done', function() {
    this.emit('record:done');
  }.bind(this));

  // emits microphone volume during mic-test
  this.udpReceiver.on('/microphone', function(e) {
    this.emit('microphone', { volume: e.params[0] });
  }.bind(this));

  // emits when mic test is done (after .startMicTest())
  this.udpReceiver.on('/mictest-done', function() {
    this.emit('mictest:done');
  }.bind(this));
}

util.inherits(Backend, EventEmitter);

/**
 * set headphones volume
 * @param {number} vol - desired volume [0..1]
 */
Backend.prototype.setHeadphonesVolume = function(vol) {
  this.udpSender.send('/volume', 'f', [ vol ]);
};

/**
 * loads song background in backend
 * requires at least 1000ms before recordSong can start
 * @param {string} path - full song path ("/Users/Audiobox/Songs/artist-title.mp3")
 */
Backend.prototype.loadSong = function(path) {
  this.udpSender.send('/load-song', 's', [ path ]);
};

/**
 * start recording singing into the microphone
 * will start sending /progress messages
 * @param {string} path - full path for output file ("/Users/Audiobox/Output/_id_.wav")
 */
Backend.prototype.recordSong = function(path) {
  this.udpSender.send('/record-start', 's', [ path ]);
};

/**
 * start automatic mic calibration
 * will start sending /microphone messages with volume
 * will send mictest-done (mictest:done) on end
 */
Backend.prototype.startMicTest = function() {
  this.udpSender.send('/mictest-start', 'T', [ true ]);
};

Backend.prototype.playRecording = function() {
  this.udpSender.send('/replay-start', 'T', [ true ]);
};

Backend.prototype.pauseRecording = function() {
  this.udpSender.send('/replay-stop', 'T', [ true ]);
};

module.exports = Backend;
