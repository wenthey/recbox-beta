var React  = require('react');

var io     = require('socket.io-client');
var socket = io.connect('http://localhost:3002');

var LiveReload = React.createClass({
  getInitialState: function() {
    return { timestamp: new Date() };
  },

  componentDidMount: function() {
    socket.on('live-css-reload:ready', function() {
      this.setState({ timestamp: new Date() });
    }.bind(this));
  },

  render: function() {
    return React.DOM.link({
      href: 'style.css?t=' + this.state.timestamp,
      rel:  'stylesheet',
      type: 'text/css'
    });
  }
});

module.exports = LiveReload;
