var async    = require('async');
var fs       = require('fs');
var path     = require('path');
var exiftool = require('exiftool');
var LRC      = require('./lrc-parser');

var audioLibraryFolder = require('../config').audioLibraryFolder;
var imagesFullFolder = require('../config').imagesFullFolder;

// mapping of extension->file[key] path
var extensionMappings = {
  png: 'image',
  jpg: 'image',
  mp3: 'audio',
  wav: 'audio',
  lrc: 'text'
};

// map of exiftool metadata to file.metadata[key]
var exifMappings = {
  album:               'album',
  artist:              'artist',
  'date/timeOriginal': 'year',
  title:               'title',
  comment:             'comment',
  genre:               'genre',
  duration:            'duration'
};

// basically to fix genre to consistent version,
// .toLowerCase is called first
var genreMappings = {
  jazz:      'jazz',
  old:       'oldies',
  oldies:    'oldies',
  pop:       'pop',
  'r n b':   'rnb',
  'r\'n\'b': 'rnb',
  rnb:       'rnb',
  unknown:   'other'
};

// file extensions accepted by db
var acceptedExtensions = Object.keys(extensionMappings);

function AudioLibrary() {
  this.library = [];
}

AudioLibrary.prototype.groupFiles = function(files, callback) {
  var grouped = files.reduce(function(memo, file) {
    var name = path.basename(file, path.extname(file));

    if (!memo[name]) { memo[name] = []; }

    memo[name].push(file);

    return memo;
  }, {});

  grouped = Object.keys(grouped).map(function(key) {
    var data = grouped[key].reduce(function(memo, file) {
      var extension = path.extname(file).replace('.', '');
      memo[extensionMappings[extension]] = path.join(imagesFullFolder, file);
      return memo;
    }, {});

    return data;
  });

  callback(null, grouped);
};

AudioLibrary.prototype.addMetadata = function(files, callback) {
  async.map(
    files,
    function(file, callback) {
      exiftool.metadata(file.audio, function(error, data) {
        if (error) {
          callback(error);
        }
        else {
          file.metadata = {};

          Object.keys(exifMappings).forEach(function(exifName) {
            var targetName = exifMappings[exifName];
            file.metadata[targetName] = data[exifName];
          });

          file.metadata.genre = undefined != file.metadata.genre ? genreMappings[file.metadata.genre.toLowerCase()] : genreMappings['unknown'];

          callback(null, file);
        }
      });
    },
    function(error, files) {
      if (error) {
        callback(error);
      }
      else {
        callback(null, files);
      }
    }
  );
};

AudioLibrary.prototype.addLRCString = function(files, callback) {
  async.map(
    files,
    function(file, callback) {
      fs.readFile(file.text, { encoding: 'utf-8' }, function(error, data) {
        if (error) {
          callback(error);
        }
        else {
          file.textString = data;
          file.textParsed = LRC(data);

          callback(null, file);
        }
      });
    },
    function(error, files) {
      if (error) {
        callback(error);
      }
      else {
        callback(null, files);
      }
    }
  );
};

AudioLibrary.prototype.addTimestamp = function(files, callback) {
  async.map(
    files,
    function(file, callback) {
      fs.stat(file.audio, function(error, stat) {
        if (error) {
          callback(error);
        }
        else {
          file.createdTime = new Date(stat.ctime);
          callback(null, file);
        }
      });
    },
    function(error, files) {
      if (error) {
        callback(error);
      }
      else {
        callback(null, files);
      }
    }
  );
};

AudioLibrary.prototype.generate = function(callback) {
  fs.readdir(audioLibraryFolder, function(error, files) {
    if (error) {
      console.error(error);
    }
    else {
      files = files.filter(function(file) {
        return acceptedExtensions.some(function(extension) {
          return file.indexOf('.' + extension) >= 0;
        });
      });

      async.waterfall([
        this.groupFiles.bind(this, files),
        this.addMetadata,
        this.addLRCString,
        this.addTimestamp
      ], function(error, results) {
        this.library = results;

        if (callback) { callback(error, results); }
      }.bind(this));
    }
  }.bind(this));
};

AudioLibrary.prototype.find = function(search, callback) {
  var searchByText = function(searchText, library) {
    searchText = searchText.toLowerCase();

    return library.filter(function(file) {
      return [ file.metadata.artist, file.metadata.album, file.metadata.title ].some(function(text) {
        return text ? text.toLowerCase().indexOf(searchText) >= 0 : false;
      });
    });
  };

  var searchByGenre = function(searchGenre, library) {
    searchGenre = searchGenre.toLowerCase();

    return library.filter(function(file) {
      return file.metadata.genre === searchGenre;
    });
  };

  var results = [];

  if (search.text) {
    results = searchByText(search.text, this.library);
  }
  else if (search.genre) {
    results = searchByGenre(search.genre, this.library);
  }

  callback(null, results);
};

AudioLibrary.prototype.listGenres = function(callback) {
  var genres = this.library.reduce(function(memo, file) {
    var genre = file.metadata.genre;
    memo[genre] = true;
    return memo;
  }, {});

  callback(null, Object.keys(genres));
};

AudioLibrary.prototype.getNewest = function(num, callback) {
  var newest = this.library
    .sort(function(a, b) {
      return b.createdTime - a.createdTime;
    })
    .slice(0, num);

  callback(null, newest);
};

module.exports = AudioLibrary;
