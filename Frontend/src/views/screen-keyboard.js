var React      = require('react');
var classNames = require('classnames');

var countdownTime = 5;

var ScreenKeyboard = React.createClass({
  getInitialState: function() {
    return {
      capitalizeNext:         false,
      capitalizeALl:          false,
      showDiacritis:          false,
      showSpecials:           false,
      showDigits:             false,
      counter:                0,
      visibleDiacriticModule: '',
      lightUpKey:             ''
    };
  },

  onClickCapitalize: function() {
    if (this.state.capitalizeAll) {
      this.setState({ capitalizeNext: false, capitalizeAll: false});
    }
    else if (this.state.capitalizeNext) {
      this.setState({ capitalizeNext: false, capitalizeAll: true });
    }
    else {
      this.setState({ capitalizeNext: true });
    }
  },

  onClickDigits: function() {
    this.setState({ showSpecials: false, showDigits: true });
  },

  onClickSpecials: function() {
    this.setState({ showSpecials: true, showDigits: false });
  },

  onClickLetters: function() {
    this.setState({ showSpecials: false, showDigits: false });
  },

  manageCapitalization: function() {
    if (this.state.capitalizeNext) {
      this.setState({ capitalizeNext: false });
    }
  },

  getKeyCode: function(key) {
    var capitalize = this.state.capitalizeNext || this.state.capitalizeAll;
    var keyCode;

    if (key.keyCode) {
      keyCode = key.keyCode;
    }
    else {
      keyCode = capitalize ? key.displayedText.toUpperCase().charCodeAt(0) :
                             key.displayedText.toLowerCase().charCodeAt(0);
    }

    return keyCode;
  },

  showKeyDiacriticsModule: function(key) {
    var diacriticsModuleParentLetter = key.displayedText || key.parentDisplayedText;
    this.setState({ visibleDiacriticModule: diacriticsModuleParentLetter });
  },

  onKeyPress: function(key) {
    var keyCode = this.getKeyCode(key);
    this.props.onKeyPress({ strValue: String.fromCharCode(keyCode), keyCode: keyCode });
    this.manageCapitalization();
  },

  count: function(key) {
    var keyPress = setInterval(function() {
      this.setState({ counter: this.state.counter + 1 });

      if (this.state.counter === countdownTime && key.diacritics) {
        this.showKeyDiacriticsModule(key);
      }
    }.bind(this), 100);

    return keyPress;
  },

  resetCounter: function() {
    clearInterval(this.state.timePressed);
    this.setState({ timePressed: 0, counter: 0 });
  },

  onMouseDown: function(key) {
    this.setState({ timePressed: this.count(key), lightUpKey: key.displayedText });

    setTimeout(function() {
      this.setState({ lightUpKey: '' });
    }.bind(this), 400);
  },

  onMouseUp: function(key) {
    if (key.action) {
      key.action();
    }
    else {
      this.onKeyPress(key);
    }
  },

  hideDiacritics: function() {
    this.setState({ visibleDiacriticModule: '' });
    this.resetCounter();
  },

  onDiacriticsMouseUp: function(diacriticKey) {
    this.hideDiacritics();
    this.onKeyPress(diacriticKey);
  },

  createDOMModule: function(module) {
    return module.map(function(row) {
      return React.DOM.div(
        { className: row.bottomDistance, key: row.bottomDistance },
        row.keys.map(function(key, index) {
          var capitalize       = this.state.capitalizeNext || this.state.capitalizeAll;
          var special          = key.special === true;
          var diacriticsModule = key.diacritics ? this.createHiddenDiacriticsModule(key) : null;

          return React.DOM.div(
            { key: index, className: 'keyboard-button-container' },
            React.DOM.div(
              {
                className:   classNames(key.classSet, { lightup: this.state.lightUpKey === key.displayedText }),
                key:         key.displayedText,
                onMouseDown: this.onMouseDown.bind(this, key),
                onMouseUp:   this.onMouseUp.bind(this, key)
              },
              (!capitalize && !special) ? key.displayedText.toLowerCase() : key.displayedText.toUpperCase()
            ),
            diacriticsModule
          );
        }.bind(this))
      );
    }.bind(this));
  },

  createHiddenDiacriticsModule: function(key) {
    return React.DOM.div(
      { className: classNames(key.diacriticsClassSet) },
      key.diacritics.map(function(diacriticKey) {
        diacriticKey.parentDisplayedText = key.displayedText;

        return React.DOM.div(
          {
            className: 'keyboard-diacritics-button-width-' + diacriticKey.width,
            key:       diacriticKey.displayedText,
            onMouseUp: this.onDiacriticsMouseUp.bind(this, diacriticKey)
          },
          diacriticKey.displayedText
        );
      }.bind(this))
    );
  },

  render: function() {
    var visibleDiacriticModule = this.state.visibleDiacriticModule;

    var modules = {
      international: [
        {
          bottomDistance: 'fourth-bottom-row',
          keys: [
            { classSet: 'letter keyboard-button-width-one', displayedText: 'Q' },
            { classSet: 'letter keyboard-button-width-one', displayedText: 'W' },
            {
              classSet: 'letter keyboard-button-width-one',
              displayedText: 'E',
              diacritics: [{ width: 'one', displayedText: 'Ę' }],
              diacriticsClassSet: { 'diacritics-container-width-1': true, visible: visibleDiacriticModule === 'E' }
            },
            { classSet: 'letter keyboard-button-width-one', displayedText: 'R' },
            { classSet: 'letter keyboard-button-width-one', displayedText: 'T' },
            { classSet: 'letter keyboard-button-width-one', displayedText: 'Y' },
            { classSet: 'letter keyboard-button-width-one', displayedText: 'U' },
            { classSet: 'letter keyboard-button-width-one', displayedText: 'I' },
            {
              classSet: 'letter keyboard-button-width-one',
              displayedText:      'O',
              diacritics:         [{ width: 'one', displayedText: 'Ó' }],
              diacriticsClassSet: { 'diacritics-container-width-1': true, visible: visibleDiacriticModule === 'O' }
            },
            { classSet: 'letter keyboard-button-width-one', displayedText: 'P' }
          ]
        },
        {
          bottomDistance: 'third-bottom-row',
          keys: [
            {
              classSet: 'letter keyboard-button-width-one',
              displayedText:      'A',
              diacritics:         [{ width: 'one', displayedText: 'Ą' }],
              diacriticsClassSet: { 'diacritics-container-width-1': true, visible: visibleDiacriticModule === 'A' }
            },
            {
              classSet: 'letter keyboard-button-width-one',
              displayedText:      'S',
              diacritics:         [{ width: 'one', displayedText: 'Ś' }],
              diacriticsClassSet: { 'diacritics-container-width-1': true, visible: visibleDiacriticModule === 'S' }
            },
            { classSet: 'letter keyboard-button-width-one', displayedText: 'D' },
            { classSet: 'letter keyboard-button-width-one', displayedText: 'F' },
            { classSet: 'letter keyboard-button-width-one', displayedText: 'G' },
            { classSet: 'letter keyboard-button-width-one', displayedText: 'H' },
            { classSet: 'letter keyboard-button-width-one', displayedText: 'J' },
            { classSet: 'letter keyboard-button-width-one', displayedText: 'K' },
            {
              classSet: 'letter keyboard-button-width-one',
              displayedText:      'L',
              diacritics:         [{ width: 'one', displayedText: 'Ł' }],
              diacriticsClassSet: { 'diacritics-container-width-1': true, visible: visibleDiacriticModule === 'L' }
            }
          ]
        },
        {
          bottomDistance: 'second-bottom-row',
          keys: [
            {
              displayedText: '-',
              action:        this.onClickCapitalize,
              special:       true,
              classSet: {
                'capitalize-button':         true,
                'keyboard-button-width-one': true,
                'capitalize-next':           this.state.capitalizeNext,
                'capitalize-all':            this.state.capitalizeAll
              }
            },
            {
              classSet: 'letter keyboard-button-width-one',
              displayedText:      'Z',
              diacritics:         [{ width: 'one', displayedText: 'Ź' }, { width: 'one', displayedText: 'Ż' }],
              diacriticsClassSet: { 'diacritics-container-width-2': true, visible: visibleDiacriticModule === 'Z' }
            },
            { classSet: 'letter keyboard-button-width-one', displayedText: 'X' },
            {
              classSet: 'letter keyboard-button-width-one',
              displayedText:      'C',
              diacritics:         [{ width: 'one', displayedText: 'Ć' }],
              diacriticsClassSet: { 'diacritics-container-width-1': true, visible: visibleDiacriticModule === 'C' }
            },
            { classSet: 'letter keyboard-button-width-one', displayedText: 'V' },
            { classSet: 'letter keyboard-button-width-one', displayedText: 'B' },
            {
              classSet: 'letter keyboard-button-width-one',
              displayedText:      'N',
              diacritics:         [{ width: 'one', displayedText: 'Ń' } ],
              diacriticsClassSet: { 'diacritics-container-width-1': true, visible: visibleDiacriticModule === 'N' }
            },
            { classSet: 'letter keyboard-button-width-one', displayedText: 'M' },
            { classSet: 'letter keyboard-button-width-one', displayedText: ',' },
            { classSet: 'letter keyboard-button-width-one', displayedText: '.' },
            {
              displayedText: '. ',
              classSet:      [ 'remove-button', 'keyboard-button-width-one' ],
              keyCode:       8,
              special:       true
            }
          ]
        }
      ],
      functionals: [
        {
          bottomDistance: 'first-bottom-row',
          keys: [
            {
              classSet: [ 'letter', 'keyboard-button-width-three' ],
              displayedText: this.state.showDigits || this.state.showSpecials ? 'ABC' : '.?123',
              action:        this.state.showDigits || this.state.showSpecials ?
              this.onClickLetters : this.onClickDigits,
              special:       true
            },
            { classSet: 'letter keyboard-button-width-six', displayedText: 'space',  keyCode: 32, special: true },
            { classSet: 'letter keyboard-button-width-two', displayedText: '@' }
          ]
        }
      ],
      digits: [
        {
          bottomDistance: 'fourth-bottom-row',
          keys: [
            { classSet: 'letter keyboard-button-width-one', displayedText: '1' },
            { classSet: 'letter keyboard-button-width-one', displayedText: '2' },
            { classSet: 'letter keyboard-button-width-one', displayedText: '3' },
            { classSet: 'letter keyboard-button-width-one', displayedText: '4' },
            { classSet: 'letter keyboard-button-width-one', displayedText: '5' },
            { classSet: 'letter keyboard-button-width-one', displayedText: '6' },
            { classSet: 'letter keyboard-button-width-one', displayedText: '7' },
            { classSet: 'letter keyboard-button-width-one', displayedText: '8' },
            { classSet: 'letter keyboard-button-width-one', displayedText: '9' },
            { classSet: 'letter keyboard-button-width-one', displayedText: '0' }
          ]
        },
        {
          bottomDistance: 'third-bottom-row',
          keys: [
            { classSet: 'letter keyboard-button-width-one', displayedText: '-'  },
            { classSet: 'letter keyboard-button-width-one', displayedText: '/'  },
            { classSet: 'letter keyboard-button-width-one', displayedText: ':'  },
            { classSet: 'letter keyboard-button-width-one', displayedText: ';'  },
            { classSet: 'letter keyboard-button-width-one', displayedText: '('  },
            { classSet: 'letter keyboard-button-width-one', displayedText: ')'  },
            { classSet: 'letter keyboard-button-width-one', displayedText: '€'  },
            { classSet: 'letter keyboard-button-width-one', displayedText: '&'  },
            { classSet: 'letter keyboard-button-width-one', displayedText: '@'  },
            { classSet: 'letter keyboard-button-width-one', displayedText: '\"' }
          ]
        },
        {
          bottomDistance: 'second-bottom-row',
          keys: [
            {
              classSet: { 'keyboard-button-width-three': true, letter: true },
              displayedText: '#+=',
              action: this.onClickSpecials,
              special: true },
            { classSet: 'letter keyboard-button-width-one',   displayedText: '.'  },
            { classSet: 'letter keyboard-button-width-one',   displayedText: ','  },
            { classSet: 'letter keyboard-button-width-one',   displayedText: '?'  },
            { classSet: 'letter keyboard-button-width-one',   displayedText: '!'  },
            { classSet: 'letter keyboard-button-width-one',   displayedText: '\'' },
            {
              displayedText: '. ',
              classSet:      [ 'remove-button', 'keyboard-button-width-one' ],
              keyCode:       8,
              special:       true
            }
          ]
        }
      ],
      specials: [
        {
          bottomDistance: 'fourth-bottom-row',
          keys: [
            { classSet: 'letter keyboard-button-width-one', displayedText: '[' },
            { classSet: 'letter keyboard-button-width-one', displayedText: ']' },
            { classSet: 'letter keyboard-button-width-one', displayedText: '{' },
            { classSet: 'letter keyboard-button-width-one', displayedText: '}' },
            { classSet: 'letter keyboard-button-width-one', displayedText: '#' },
            { classSet: 'letter keyboard-button-width-one', displayedText: '%' },
            { classSet: 'letter keyboard-button-width-one', displayedText: '^' },
            { classSet: 'letter keyboard-button-width-one', displayedText: '*' },
            { classSet: 'letter keyboard-button-width-one', displayedText: '+' },
            { classSet: 'letter keyboard-button-width-one', displayedText: '=' }
          ]
        },
        {
          bottomDistance: 'third-bottom-row',
          keys: [
            { classSet: 'letter keyboard-button-width-one', displayedText: '_'  },
            { classSet: 'letter keyboard-button-width-one', displayedText: '\\' },
            { classSet: 'letter keyboard-button-width-one', displayedText: '|'  },
            { classSet: 'letter keyboard-button-width-one', displayedText: '~'  },
            { classSet: 'letter keyboard-button-width-one', displayedText: '<'  },
            { classSet: 'letter keyboard-button-width-one', displayedText: '>'  },
            { classSet: 'letter keyboard-button-width-one', displayedText: '$'  },
            { classSet: 'letter keyboard-button-width-one', displayedText: '£'  },
            { classSet: 'letter keyboard-button-width-one', displayedText: '¥'  }
          ]
        },
        {
          bottomDistance: 'second-bottom-row',
          keys: [
            {
              classSet: [ 'letter', 'keyboard-button-width-three' ],
              displayedText: '123',
              action: this.onClickDigits,
              special: true
            },
            { classSet: 'letter keyboard-button-width-one',   displayedText: '.'  },
            { classSet: 'letter keyboard-button-width-one',   displayedText: ','  },
            { classSet: 'letter keyboard-button-width-one',   displayedText: '?'  },
            { classSet: 'letter keyboard-button-width-one',   displayedText: '!'  },
            { classSet: 'letter keyboard-button-width-one',   displayedText: '\'' },
            {
              displayedText: ' .',
              classSet:      [ 'remove-button', 'keyboard-button-width-one' ],
              keyCode:       8,
              special:       true
            }
          ]
        }
      ]
    };

    var defaultKeys = this.createDOMModule(modules.international);
    var functionals = this.createDOMModule(modules.functionals);
    var digits      = this.createDOMModule(modules.digits);
    var specials    = this.createDOMModule(modules.specials);

    return React.DOM.div(
      { className: 'keyboard-container stop-select', onMouseUp: this.hideDiacritics },
      this.state.showDigits ? digits : null,
      this.state.showSpecials ? specials : null,
      !this.state.showDigits && !this.state.showSpecials ? defaultKeys : null,
      functionals,
      React.DOM.div({ className: 'capitalize-next-background-preloader' }),
      React.DOM.div({ className: 'capitalize-all-background-preloader' })
    );
  }
});

module.exports = ScreenKeyboard;
