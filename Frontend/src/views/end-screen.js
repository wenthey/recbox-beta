var React            = require('react');
var dataCollector    = require('../data-collector');
var endScreenTimeout = require('../../config').endScreenTimeout;

var EndScreen = React.createClass({
  componentWillUnmount: function() {
    dataCollector.sessionEnd();
  },

  componentDidMount: function() {
    this.timeoutHandler = setTimeout(function() {
      this.props.stateMachine.transition('timeout');
    }.bind(this), endScreenTimeout);
  },

  componentDidUnmount: function() {
    clearTimeout(this.timeoutHandler);
  },

  onClickRestart: function() {
    this.props.stateMachine.transition('login');
  },

  render: function() {
    return React.DOM.div(
      { className: 'end-screen-container' },
      React.DOM.div({ className: 'main-header' }, 'Dziękujemy za odwiedziny.'),
      React.DOM.div(
        { className: 'sub-header mail-info'},
        'W ciągu maksymalnie 12 godzin, na podany adres email otrzymasz wiadomość z łączem do pobrania nagrania.'
      ),
      React.DOM.div({ className: 'sub-header see-you-message' }, 'Do zobaczenia wkrótce...mamy nadzieję ;)'),
      React.DOM.div({ className: 'start-again', onClick: this.onClickRestart }, 'Zacznij od nowa')
    );
  }
});

module.exports = EndScreen;
