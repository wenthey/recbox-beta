var React         = require('react');
var async         = require('async');
var extend        = require('extend');
var dataCollector = require('../../data-collector');

var HorizontalDragSlider = React.createFactory(require('./horizontal-drag-slider'));
var VerticalDragSlider = React.createFactory(require('./vertical-drag-slider'));

var genreImagesMap  = {
  pop:    'images/genre-pop.png',
  rnb:    'images/genre-rnb.png',
  oldies: 'images/genre-oldies.png',
  rock:   'images/genre-rock.png',
  jazz:   'images/genre-jazz.png'
};

var Genres = React.createClass({
  getInitialState: function() {
    return {
      genres: []
    };
  },

  componentWillMount: function() {
    this.props.db.genres.find({}).sort({order: 1}).exec(function (err, docs) {
      // this.setState({genres: docs.map(function(genre){ return genre.text; })});
      this.setState( { genres: docs } );
    }.bind(this));
  },

  onClickGenre: function (genre, genreText) {
    this.props.db.songs
      .find({genre: genre})
      .exec(function(err, docs){
        if (err) { console.log(err); return; } 

        docs = docs.map(function(doc) {
          var songDuration = doc.duration;
          //doc.duration = songDuration.replace(' (approx)', '');
          return doc;
      });

      this.props.stateMachine.transition( // @FIXME: wywolanie tego daje blad: Uncaught Error: ENOENT: no such file or directory, rename './db/songs.db~' -> './db/songs.db'
          'search-results', 
          {
            songs: docs,
            searchedGenre: genreText
          }
      );
    }.bind(this));
  },

  render: function() {
    var genres = this.state.genres;
    return React.DOM.div(
      { className: 'genres-container' },
      React.DOM.ul(
        { className: 'genres-list' },
        genres.map(function (el) { 
          return React.DOM.li(
            { className: 'genres-list-item', key: el._id },
            React.DOM.div(
              { onClick: this.onClickGenre.bind(this, el.key, el.text) },
              el.text
            )
          ) 
        }.bind(this) )
      )
    );
  }   
});

module.exports = Genres;
