var React = require('react');
var dataCollector = require('../../data-collector');
var extend = require('extend');
var ImagesFolder = require('../../../config').imagesFullFolder;
var AudioLibraryFolder = require('../../../config').audioLibraryFolder;


var SearchResults = React.createClass({
    onClickSing: function () {
        dataCollector.selectedSong({
            artist: this.props.data.song.artist,
            title: this.props.data.song.title
        });

        var songPath = AudioLibraryFolder + '/' + this.props.data.song.fullAudio;
        this.props.backend.loadSong(songPath);

        var data = extend({}, this.props.data);
        this.props.stateMachine.transition('sing', data);
    },

    render: function () {
        return React.DOM.div(
            {
                className: 'song-details-container',
                style: {
                    background: 'url("' + ImagesFolder + '/' + this.props.data.song.imageFullSizeURL + '") no-repeat center',
                    backgroundSize: 'cover !important',
                    backgroundColor: 'rgba(23,24,23,0.8)',
                    backgroundBlendMode: 'overlay'
                }
            },
            React.DOM.div(
                {className: 'info-container'},
                React.DOM.div(
                    {className: 'main-header'},
                    React.DOM.div({className: 'artist'}, this.props.data.song.artist),
                    React.DOM.div({className: 'title'}, this.props.data.song.title)
                ),
                React.DOM.div(
                    {className: 'btn-top'},
                    //React.DOM.img({className: 'sing-button', src: 'images/rb-btn-zaspiewaj.svg', onClick: this.onClickSing})
                    React.DOM.div({onClick: this.onClickSing}, 'Rozpocznij nagranie')

                )
            ),
            React.DOM.div(
                {className: 'track-info'},
                React.DOM.div(
                    {className: 'length-container'},
                    React.DOM.span({className: 'sub-info-title'}, 'Czas trwania'),
                    React.DOM.span({className: 'sub-info-description'}, this.props.data.song.duration.replace('(approx)', ''))
                ),
                React.DOM.div(
                    {className: 'length-container'},
                    React.DOM.span({className: 'sub-info-title'}, 'Język tekstu'),
                    React.DOM.span({className: 'sub-info-description'}, this.props.data.song.language)
                ),
                React.DOM.div(
                    {className: 'length-container'},
                    React.DOM.span({className: 'sub-info-title'}, 'Gatunek'),
                    React.DOM.span({className: 'sub-info-description'}, this.props.data.song.genre)
                )
            )
        );
    }
});

module.exports = SearchResults;
