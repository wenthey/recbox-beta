var React             = require('react');
var screenRatioFactor = require('../../../config').screenRatioFactor;
var Draggable         = React.createFactory(require('react-draggable'));

var DragSlider = React.createClass({
  getDefaultProps: function() {
    return {
      genreSliderMargin: 10,
      genreSliderHeight: 225
    };
  },

  render: function() {
    var numItems          = this.props.children.length;
    var genreSliderMargin = this.props.genreSliderMargin * screenRatioFactor;
    var genreSliderHeight = this.props.genreSliderHeight * screenRatioFactor + 2 * genreSliderMargin;
    var topBound          = (genreSliderHeight + 2 * genreSliderMargin) * (numItems + 2);

    return React.DOM.div(
      { className: 'library-vertical-drag-slider' },
      Draggable(
        { axis: 'y', bounds: { top: -topBound, bottom: 0 } },
        React.DOM.div(
          { className: 'vertical-drag-container' },
          this.props.children.map(function(child, i) {
            return React.DOM.div({ key: i, className: 'vertical-drag-child' }, child);
          })
        )
      )
    );
  }
});

module.exports = DragSlider;
