var React             = require('react');
var screenRatioFactor = require('../../../config').screenRatioFactor;
var Draggable         = React.createFactory(require('react-draggable'));

var DragSlider = React.createClass({
  getDefaultProps: function() {
    return {
      miniatureMargin: 10,
      miniatureWidth:  445
    };
  },

  render: function() {
    var numItems        = this.props.children.length;
    var miniatureMargin = this.props.miniatureMargin * screenRatioFactor;
    var miniatureWidth  = this.props.miniatureWidth * screenRatioFactor + 2 * miniatureMargin;
    var miniaturesWidth = miniatureWidth * numItems;
    var leftBound       = 1920 * screenRatioFactor - miniaturesWidth;

    return React.DOM.div(
      { className: 'library-drag-slider' },
      Draggable(
        { axis: 'x', bounds: { right: 0, left: leftBound } },
        React.DOM.div(
          { className: 'drag-container' },
          this.props.children.map(function(child, i) {
            return React.DOM.div({ key: i, className: 'drag-child' }, child);
          })
        )
      )
    );
  }
});

module.exports = DragSlider;
