var React         = require('react');
var Menu          = React.createFactory(require('./menu'));
var SongDetails   = React.createFactory(require('./song-details'));
var SongSearch    = React.createFactory(require('./song-search'));
var SearchResults = React.createFactory(require('./search-results'));
var Newest        = React.createFactory(require('./newest'));
var Genres        = React.createFactory(require('./genres'));

var LibraryManager = React.createClass({
  render: function() {
    var LibraryViews = {
      'song-search':    SongSearch,
      'search-results': SearchResults,
      'song-details':   SongDetails,
      'library-newest': Newest,
      'library-genres': Genres
    };

    return React.DOM.div(
      { className: 'library-manager-container' },
      Menu({
        currentState: this.props.currentState,
        stateMachine: this.props.stateMachine
      }),
      LibraryViews[this.props.currentState]({
        stateMachine:     this.props.stateMachine,
        backend:          this.props.backend,
        data:             this.props.data,
        currentState:     this.props.currentState,
        audioLibrary:     this.props.audioLibrary,
        db:               this.props.db,
        globalContainer:  this.props.globalContainer
      })
    );
  }
});

module.exports = LibraryManager;
