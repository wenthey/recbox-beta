var React         = require('react');
var dataCollector = require('../../data-collector');
var Keyboard      = React.createFactory(require('../screen-keyboard'));

var SongSearch = React.createClass({
  getInitialState: function() {
    var storedInputValue = this.props.globalContainer.get('song_search_input_value');

    return {
      searchValue: undefined != storedInputValue ? storedInputValue : 'like a beast'
    };
  },

  onClickSearch: function() {

    function escapeRegExp(str) {
      return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
    }

    function modifyForSmartSearch(str) {
      // only 2 letters or more
      return str.length >= 2 ? str.split('').join('.?') : str;
    }

    dataCollector.searchForSong(this.state.searchValue);
    // var regex = new RegExp(modifyForSmartSearch(escapeRegExp(this.state.searchValue)), "i");

    var searchList = this.state.searchValue.split(new RegExp(/[\s,+-]+/))

    searchClearList = searchList.filter(function (item) {
      return item.length > 2;
    });

    searchRegexList = searchClearList.map(function (item) {
      return new RegExp(modifyForSmartSearch(escapeRegExp(item)), "i")
    });

    this.props.db.songs
      .find({ 
        $where: function () { 
          if (0 == searchRegexList.length) { return true; }

          var artistFounds = searchRegexList.map(function (f1) {
            return this.artist.search(f1) > -1 ? 1 : 0;
          }.bind(this));

          var titleFounds = searchRegexList.map(function (f2) {
            return this.title.search(f2) > -1 ? 1 : 0;
          }.bind(this));

          var artistFoundsSum = artistFounds.reduce( (prev, curr) => prev + curr );
          var titleFoundsSum = titleFounds.reduce( (prev, curr) => prev + curr );

          // double points if found in title and title
          this.searchScore = artistFoundsSum + titleFoundsSum;

          if (artistFoundsSum > 0 && titleFounds > 0) {
            this.searchScore = 2 * this.searchScore;
          }

          return this.searchScore > 0; 
        }
      })
      .sort({ searchScore: -1 })
      .exec(function(err, docs){
        if (err) { console.log(err); return; } 

        docs = docs.map(function(doc) {
          var songDuration = doc.duration;
          //doc.duration = songDuration.replace(' (approx)', '');
          return doc;
      });

      this.props.stateMachine.transition( // @FIXME: wywolanie tego daje blad: Uncaught Error: ENOENT: no such file or directory, rename './db/songs.db~' -> './db/songs.db'
          'search-results',
          {
            searchedValue: this.state.searchValue,
            songs: docs
          }
      );
    }.bind(this));
  },

  onKeyPress: function(event) {
    var searchValue = this.state.searchValue;

    if (event.keyCode === 8) {
      // delete last char
      searchValue = searchValue.substring(0, Math.max(searchValue.length - 1, 0));
    }
    else {
      searchValue += event.strValue;
    }

    this.props.globalContainer.save({song_search_input_value: searchValue});
    this.setState({ searchValue: searchValue });

    // enter - same as clicking 'search'
    if (event.keyCode === 13) {
      this.onClickSearch();
    }
  },

  render: function() {
    return React.DOM.div(
      { className: 'song-search-container' },
      Keyboard({ onKeyPress: this.onKeyPress }),
      React.DOM.div({ onClick: this.onClickSearch, className: 'rb-btn' }, 'SZUKAJ'),
      React.DOM.div(
        { className: 'search-form-container' },
        React.DOM.input({
          className:   'song-search-input',
          placeholder: 'Wpisz artystę, tytuł lub gatunek utworu...',
          value:       this.state.searchValue,
          readOnly:    true
        })

      )
    );
  }
});

module.exports = SongSearch;
