var React = require('react');
var extend = require('extend');

var Slider = React.createFactory(require('react-slick'));
var ImagesFolder = require('../../../config').audioLibraryFolder;

var Newest = React.createClass({
    getInitialState: function () {
        return {
            newest: [],
            sliderInnerWidth: 0,
            genres: []
        };
    },

    componentWillMount: function () {
        this.props.db.songs.find({}).sort({dateAdded: -1}).exec(function (err, docs) {
            this.setState({newest: docs});
            this.setSliderInnerWidth();
        }.bind(this));

        //this.props.db.genres.find({}).sort({order: 1}).exec(function (err, docs) {
        //    this.setState({genres: docs});
        //}.bind(this)); // @TODO - remove genres class as it is not needed anymore
    },

    //onClickGenre: function (genre) {
    //    this.props.db.songs.find({genre: genre}).exec(function(err, docs){
    //        this.setState({newest: docs});
    //        this.setSliderInnerWidth();
    //    }.bind(this));
    //},

    //onClickGenre: function (genre) {
    //    this.props.audioLibrary.find({genre: genre}, function (error, results) {
    //        if (error) {
    //            return;
    //        }
    //
    //        results = results.map(function (result) {
    //            var songDuration = result.metadata.duration;
    //            result.metadata.duration = songDuration.replace(' (approx)', '');
    //
    //            return result;
    //        });
    //
    //        var data = extend({}, this.props.data, {songs: results, searchedValue: genre});
    //
    //        this.props.stateMachine.transition('genre', data);
    //    }.bind(this));
    //},

    onClickSing: function (song) {
        var data = extend({}, this.props.data, {song: song});
        this.props.stateMachine.transition('song-details', data);
    },

    onClickMiniature: function (song) {
        var data = extend({}, this.props.data, {song: song});
        this.props.stateMachine.transition('song-details', data);
    },

    setSliderInnerWidth: function () {
        var elemWidth = 456;
        console.log(this.state.newest);
        console.log(elemWidth);
        console.log(this.state.newest.length * elemWidth);
        this.setState({sliderInnerWidth: this.state.newest.length * elemWidth});
    },

    renderSliderSongs: function (songs) {
        console.log('slider rendered');
        console.log(songs);
        return songs.map(function (song, index) {

            var key = song.artist + '-' + song.title + '-' + index;

            return React.DOM.div(
                {
                    className: 'newest-song',
                    key: key,
                    style: {backgroundImage: 'url(\'' + ImagesFolder + '/' + song.imageFullSizeURL + '\')'}
                },
                React.DOM.div(
                    {className: 'bottom-slide-stripe'},
                    React.DOM.span({className: 'artist', onClick: this.onClickSing.bind(this, song)}, song.artist),
                    React.DOM.span({className: 'title', onClick: this.onClickSing.bind(this, song)}, song.title)
                )
            );
        }.bind(this));
    },

    renderSlider: function (songs) {
        return React.DOM.div(
            {className: 'library-newest-slides'},
            Slider(// @FIXME cos tu sie psuje i sie slider nie laduje
                {
                    arrows: false,
                    autoplay: true,
                    autoplaySpeed: 5000,
                    dots: true,
                    infinite: true,
                    speed: 800,
                    slidesToShow: 1,
                    slidesToScroll: 1
                },
                this.renderSliderSongs(songs)
            )
        )
    },

    renderSongPicker: function () {
        return React.DOM.div(
            {className: 'library-newest-simple-slider'},
            React.DOM.div(
                {
                    className: 'library-newest-simple-slider-inner',
                    style: {
                        width: this.state.sliderInnerWidth,
                        display: 0 == this.state.sliderInnerWidth ? 'none' : 'block'
                    }
                },
                this.state.newest.map(function (song) {
                    return React.DOM.div(
                        {
                            className: 'song-miniature',
                            style: {backgroundImage: 'url(\'' + ImagesFolder + '/' + song.imageThumbnailURL + '\')'},
                            onClick: this.onClickMiniature.bind(this, song)
                        },
                        React.DOM.div(
                            {className: 'miniature-description'},
                            React.DOM.div(
                                {
                                    className: 'miniature-artist',
                                    style: {textShadow: '0 1px 4px rgba(0,0,0,0.5)'}
                                }, song.artist),
                            React.DOM.div(
                                {
                                    className: 'miniature-title',
                                    style: {textShadow: '0 1px 4px rgba(0,0,0,0.5)'}
                                }, song.title)
                        )
                    )
                }.bind(this))
            )
        );
    },

    //renderGenresStripe: function () {
    //
    //    return React.DOM.div(
    //        {className: 'bottom-genres-stripe'},
    //        this.state.genres.map(function (data) {
    //            return React.DOM.div(
    //                {className: 'genre', onClick: this.onClickGenre.bind(this, data.key), 'key': data.key},
    //                //React.DOM.img({src: data.image}), // @TODO - remove genres images as this class is not needed anymore.
    //                React.DOM.br(),
    //                React.DOM.span(null, data.text)
    //            );
    //        }.bind(this))
    //    )
    //},

    render: function () {
        return React.DOM.div(
            {className: 'library-container'},
            this.renderSlider(this.state.newest),
            this.renderSongPicker()
            //this.renderGenresStripe()
        );
    }
});

module.exports = Newest;
