var React  = require('react');
var Slider = React.createFactory(require('./slider'));

var Footer = React.createClass({
  render: function() {
    return React.DOM.div(
      { className: 'footer-container' },
      React.DOM.span({ className: 'artist' }, this.props.artist),
      React.DOM.span({ className: 'title' }, this.props.title),
      Slider({ onVolumeChange: this.props.onVolumeChange })
    );
  }
});

module.exports = Footer;
