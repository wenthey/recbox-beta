var React       = require('react/addons');
var classNames  = require('classnames');
var QuitView = React.createFactory(require('../quit'));


var Menu = React.createClass({
  getInitialState: function() {
    return {
      loginEmail: 'office@mgorski.com',
      finishSessionModal: false
    };
  },

  onClickNewest: function() {
    this.props.stateMachine.transition('newest');
  },

  onClickGenres: function() {
    this.props.stateMachine.transition('genres');
  },

  onClickSearch: function() {
    this.props.stateMachine.transition('song-search');
  },


  onClickQuit: function() {
    this.setState({ finishSessionModal: true }); 
  },
  abortSession: function() {
    this.props.stateMachine.transition('quit');
  },
  stopExiting: function() {
    this.setState({ finishSessionModal: false });     
  },

  renderQuitView: function() {

    if (undefined != this.state.loginEmail) {
      return React.DOM.li(
        { },
        React.DOM.div(
          { className: 'menu-abort', onClick:   this.onClickQuit },
          '(' + this.state.loginEmail + ') - Wyjdź'
        ),
        true == this.state.finishSessionModal
          ? QuitView({ abortSession: this.abortSession, stopExiting: this.stopExiting })
          : null
      );
    } else {
      return null;
    }
  },

  watchLogin: function() {
    var storedLoginEmailValue = this.props.globalContainer.get('login_email_input_value');
    if (undefined != storedLoginEmailValue) {
      this.setState({loginEmail: storedLoginEmailValue});
    }
  },

  render: function() {
    var currentView = this.props.currentState;

    var isSearchView = [ 'song-search', 'search-results' ].some(function(viewName) {
      return currentView === viewName;
    });

    var isNewestView = currentView === 'library-newest';
    var isGenresView = currentView === 'library-genres';

    return React.DOM.div(
      { className: 'library-menu-container' },
      React.DOM.div({ className: 'rb-logo' }),
      React.DOM.ul(
        { className: 'library-menu' },
        React.DOM.li({
          className: classNames({ red: isNewestView }),
          onClick:   this.onClickNewest
        }, 'Najnowsze'),
        React.DOM.li({
          className: classNames({ red: isGenresView }),
          onClick:   this.onClickGenres
        }, 'Gatunki'), // @TODO - Generate a view for this. Need a list of geners NAMES that I can style action onclick > go to search for clicked genre
        React.DOM.li(
          { className: 'search-container'},
          //React.DOM.div({
          //  className: classNames('search-icon', { red: isSearchView }),
          //  onClick:   this.onClickSearch
          //}),
          React.DOM.div({
            className: classNames('search', { red: isSearchView }),
            onClick:   this.onClickSearch
          }, 'Wyszukaj')
        ),
        this.renderQuitView()
      )
    );
  }
});

module.exports = Menu;
