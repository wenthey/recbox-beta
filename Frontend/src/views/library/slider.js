var React = require('react');
var Slider = React.createFactory(require('rc-slider'));

var Karaoke = React.createClass({
  getInitialState: function() {
    return { volume: 100 };
  },

  onChangeVolume: function(v) {
    this.setState({ volume: v });
    this.props.onVolumeChange(this.state.volume / 100.0);
  },

  onClickMute: function() {
    this.setState({ volume: 0 });
    // TODO: tweak rc-slider package so it allows to mute
    // (set .rc-sider-handle to left: 0% and .rc-slider-track width: 0% )
  },

  render: function() {
    return React.DOM.div({ className: 'rc-slider-container' },
      // React.DOM.img({ className: 'mute', src: 'images/mute.png', onClick: this.onClickMute }),
      Slider({ onChange: this.onChangeVolume, defaultValue: this.state.volume }),
      React.DOM.span({ className: 'volume' }, this.state.volume + '%')
    );
  }
});

module.exports = Karaoke;
