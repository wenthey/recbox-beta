var React      = require('react');
var classNames = require('classnames');

var SearchResults = React.createClass({
  onClickSongDetails: function(song) {
    this.props.data.song = song;
    this.props.stateMachine.transition('song-details', this.props.data);
  },

  renderResults: function(songs) {
    if (songs) {
      return songs.map(function(song, index) {
        return React.DOM.li(
          { className: 'song', key: index },
          React.DOM.span({ className: 'artist', onClick: this.onClickSongDetails.bind(this, song) }, song.artist),
          React.DOM.span({ className: 'title', onClick: this.onClickSongDetails.bind(this, song) }, song.title),
          //React.DOM.div(
          //  { className: 'length-container' },
          //  React.DOM.span({className: 'sub-info-title'}, 'Czas trwania'),
          //  React.DOM.span({ className: 'sub-info-description'}, song.duration)
          //),
          //React.DOM.div(
          //  { className: 'date-container' },
          //  React.DOM.span({className: 'sub-info-title'}, 'Język tekstu'),
          //  React.DOM.span({className: 'sub-info-description'}, this.props.data.song.language)
          //),
          // React.DOM.div(
          //   { className: 'factory-container' },
          //   React.DOM.img({ className: 'vinyl', src: 'images/vinyl.png' }),
          //   React.DOM.span({ className: 'name'}, 'PRODUCENT TU')
          // ),
          React.DOM.div({
            className: 'details-button',
            onClick: this.onClickSongDetails.bind(this, song)
          }, 'Przejdź do utworu')
        );
      }.bind(this));
    }
  },

  renderHeaderTextA: function() {
    console.log(this.props);
    var text = '';
    if (undefined != this.props.data.searchedValue) {
      text = 'Wyniki wyszukiwania dla "' + this.props.data.searchedValue + '" - ';
    } else if (undefined != this.props.data.searchedGenre) {
      text = 'Utwory z gatunku "' + this.props.data.searchedGenre + '" - ';
    }
    return text;
  },

  render: function() {
    return React.DOM.div(
      { className: 'search-results-container stop-select' },
      React.DOM.div(
        { className: 'header' },
        this.renderHeaderTextA(),
        'liczba znalezionych utworów: ',
        this.props.data.songs.length
      ),
      React.DOM.ul(
        { className: 'songs-list' },
        this.renderResults(this.props.data.songs)
      )
    );
  }
});

module.exports = SearchResults;
