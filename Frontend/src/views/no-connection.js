var React = require('react');

var NoConnection = React.createClass({
  render: function() {
    return React.DOM.div(
      { className: 'no-connection-container' },
      React.DOM.span({ className: 'message' }, 'Sorry, no internet connection available at this moment. Try later.')
    );
  }
});

module.exports = NoConnection;


