var React             = require('react');
var remap             = require('re-map');
var screenRatioFactor = require('../../Config').screenRatioFactor;

var clamp = function(v, min, max) {
  return Math.min(max, Math.max(v, min));
};

var MicTestAnimation = React.createClass({
  getDefaultProps: function() {
    return {
      volume: 0,
      width:  600,
      height: 600
    };
  },

  render: function() {
    var width     = this.props.width;
    var height    = this.props.height;

    var minVolume = -60;
    var maxVolume = 10;
    var volume    = remap(this.props.volume, minVolume, maxVolume, 0, 1);

    var circles = [
      { range: [ 0,     1 / 3 ], size: [ 380, 720  ], opacity: 0.15 },
      { range: [ 1 / 3, 2 / 3 ], size: [ 380, 1060 ], opacity: 0.10 },
      { range: [ 2 / 3, 1     ], size: [ 380, 1400 ], opacity: 0.05 }
    ].map(function(data) {
      var size = clamp(remap(volume, data.range[0], data.range[1], 0, 1), 0, 1);
      var r    = (data.size[0] + size * (data.size[1] - data.size[0])) * screenRatioFactor;

      return React.DOM.circle({
        className: 'volume-circle',
        cx:        width / 2,
        cy:        height / 2,
        r:         r / 2,
        opacity:   data.opacity
      });
    });

    return React.DOM.div(
      { className: 'mic-test-animation' },
      React.DOM.svg(
        { width: width, height: height },
        circles
      )
    );
  }
});

module.exports = MicTestAnimation;
