var React               = require('react');
var screenRatioFactor   = require('../../config').screenRatioFactor;

var TransitionAnimation = React.createFactory(require('./transition-animation'));

var Transition = React.createClass({
  getInitialState: function() {
    return {
      stage: this.props.stateMachine.getCurrentStateObject()
    };
  },

  getStageState: function(checkedStateName) {
    var states = [
      'login',
      'library-newest',
      'mic-test',
      'karaoke',
      'end-screen'
    ];

    var stageState;
    var currentStateIndex = states.indexOf(this.state.stage.name);
    var checkedStateIndex = states.indexOf(checkedStateName);

    if (checkedStateIndex < currentStateIndex) {
      stageState = 'passed';
    }
    else if (checkedStateIndex === currentStateIndex) {
      stageState = 'next';
    }
    else if (checkedStateIndex > currentStateIndex) {
      stageState = 'future';
    }

    return stageState;
  },

  getCurrentProgressLineWidth: function(currentStageNumber) {
    var singleDOMStagePercantageWidth = 17;
    return singleDOMStagePercantageWidth * (currentStageNumber - 1) + '%';
  },

  render: function() {
    var currentStageNumber = 0;
    var currentProgressLineWidth = 0;
    var stageState;

    var stages = [
      { viewNumber: 1, state: '',               headerFirstLine: 'Witaj',     headerSecondLine: 'w RecBox' },
      { viewNumber: 2, state: 'login',          headerFirstLine: 'Podaj',     headerSecondLine: 'adres email'    },
      { viewNumber: 3, state: 'library-newest', headerFirstLine: 'Wybierz',   headerSecondLine: 'utwór'       },
      { viewNumber: 4, state: 'mic-test',       headerFirstLine: 'Przygotuj', headerSecondLine: 'mikrofon'    },
      { viewNumber: 5, state: 'karaoke',        headerFirstLine: 'Czas na',   headerSecondLine: 'nagranie'    },
      { viewNumber: 6, state: 'end-screen',     headerFirstLine: 'Wyślij',    headerSecondLine: 'nagranie'    }
    ].map(function(stage) {
      stageState = this.getStageState(stage.state);
      if (stageState === 'next') {
        currentStageNumber = stage.viewNumber;
      }

      return React.DOM.div(
        { key: stage.viewNumber, className: 'stage-element' },
        React.DOM.div({ className: 'digit ' + stageState }, stage.viewNumber),
        React.DOM.div({ className: 'header'}, stage.headerFirstLine),
        React.DOM.div({ className: 'header'}, stage.headerSecondLine)
      );
    }.bind(this));

    currentProgressLineWidth = this.getCurrentProgressLineWidth(currentStageNumber);

    return React.DOM.div(
      { key: 'transition-screen', className: 'transition-container stop-select' },
      React.DOM.div(
        { className: 'timeline' },
        React.DOM.div({ className: 'progress-line-grey'}),
        React.DOM.div({ className: 'progress-line-red', style: { width: currentProgressLineWidth }}),
        stages
      ),
      TransitionAnimation({
        width:  1920 * screenRatioFactor,
        height: 720  * screenRatioFactor
      }),
      React.DOM.div({ className: 'loading-text'}, 'Przygotowuję następny krok...')
    );
  }
});

module.exports = Transition;
