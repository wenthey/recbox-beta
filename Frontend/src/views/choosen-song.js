var React         = require('react');
var dataCollector = require('../data-collector');
var uploadManager = require('../upload-manager');

var CancelView    = React.createFactory(require('./cancel'));
var Footer        = React.createFactory(require('./library/footer'));
var ImagesFolder = require('../../config').imagesFullFolder;

var ChoosenSong = React.createClass({
  getInitialState: function() {
    return {
      finishSessionModalVisible: false
    };
  },

  componentWillUnmount: function() {
    this.props.backend.pauseRecording();
  },

  onClickSend: function() {
    uploadManager.uploadFile(this.props.data.recordPath, {
      artist: this.props.data.song.artist,
      email:  this.props.data.email,
      title:  this.props.data.song.title
    });

    this.props.stateMachine.transition('end-screen');
  },

  onClickListen: function() {
    this.props.backend.playRecording();
  },

  onClickEndSession: function() {
    this.setState({ finishSessionModalVisible: true });
  },

  onVolumeChange: function(v) {
    this.props.backend.setHeadphonesVolume(v);
  },

  abortSession: function() {
    this.props.stateMachine.transition('restart');
  },

  stopExiting: function() {
    this.setState({ finishSessionModalVisible: false });
  },

  renderFinishInfo: function() {
    return React.DOM.div(
      null,
      React.DOM.div({ className: 'main-header' }, 'Poszło Ci wspaniale!'),
      React.DOM.div(
        { className: 'sub-header'},
        'Co chcesz dalej zrobić?'
      ),
      React.DOM.div(
        { className: 'buttons-container'},
        React.DOM.div({ className: 'send-recording', onClick: this.onClickSend }, 'Wyślij nagranie'),
        React.DOM.div({ className: 'abort-recording', onClick: this.onClickEndSession }, 'Wyjdź')
      )
    );
  },

  render: function() {
    return React.DOM.div(
      {
        className: 'choosen-song-container stop-select',
        //style: {
        //  backgroundSize: 'cover !important', // FIXME: '!important', really? and this should be in less if it's static
        //  background:     'url("' + ImagesFolder + '/' + this.props.data.song.imageFullSizeURL + '") no-repeat center'
        //  // FIXME: ^ 'no-repeat center' should be in less
        //}
      },
      React.DOM.div({ className: 'black-aple'}),
      React.DOM.div({ className: 'end-session rb-hide', onClick: this.onClickEndSession }),
      this.state.finishSessionModalVisible ? CancelView({
        abortSession: this.abortSession,
        stopExiting:  this.stopExiting
      }) : this.renderFinishInfo()
    );
  }
});

module.exports = ChoosenSong;
