var React = require('react');

var QuitView = React.createClass({
  render: function() {
    return React.DOM.div(
      { 
        className: 'cancel-view-container',
        style: {position: 'fixed', top: 0, left: 0, width: '100%', height: '100%'}
      }, 
      React.DOM.div({ className: 'main-header' }, 'Na pewno chcesz wyjść?'),
      React.DOM.div(
        { className: 'buttons-container'},
        React.DOM.div({ className: 'abort-recording', onClick: this.props.abortSession }, 'Tak'),
        React.DOM.div({ className: 'send-recording', onClick: this.props.stopExiting }, 'Jeszcze nie')
      )
    );
  }
});

module.exports = QuitView;
