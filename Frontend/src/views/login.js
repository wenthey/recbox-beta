var React         = require('react/addons');
var classNames    = require('classnames');
var dataCollector = require('../data-collector');

var Keyboard      = React.createFactory(require('./screen-keyboard'));

var Login = React.createClass({
  getInitialState: function() {
   return {
      email:       'office@mgorski.com',
      invalidMail: false
    };
  },

  componentDidMount: function() {
    // @todo: there have to be better place for this line
    this.props.globalContainer.flushStorage();

    dataCollector.sessionStart();
  },

  onClickLogin: function() {
    var isValidMail = function(mail) {
      return mail.indexOf('@') > 0;
    };

    if (isValidMail(this.state.email)) {
      this.setState({ invalidMail: false });
      this.props.globalContainer.save({login_email_input_value: this.state.email});
      this.props.stateMachine.transition('library-newest', { email: this.state.email });
    }
    else {
      this.setState({ invalidMail: true });
    }
  },

  onKeyPress: function(event) {
    var email = this.state.email;

    if (event.keyCode === 8) {
      // delete last char
      email = email.substring(0, Math.max(email.length - 1, 0));
    }
    else {
      email += event.strValue;
    }

    this.setState({ email: email });

    // enter - same as clicking 'login'
    if (event.keyCode === 13) {
      this.onClickLogin();
    }
  },

  render: function() {


    return React.DOM.div(
      { className: 'login-view-container' },
      Keyboard({ onKeyPress: this.onKeyPress }),
        React.DOM.div({ className: 'rb-logo', style: {marginTop: '10px'} }), // single Logo implemetation to teh current view REMEMBER: to render that part in every view without menu
        React.DOM.div({ onClick: this.onClickLogin, className: 'rb-btn' }, 'Przejdź dalej'),
        React.DOM.div(
        { className: 'login-form-container' },
        React.DOM.input({
          className:   classNames('login-name-input'),
          placeholder: 'Wpisz adres email, na który ma chcesz otrzymać nagranie...',
          value:       this.state.email,
          readOnly:    true
        }),
        this.state.invalidMail ? React.DOM.span(
          { className: 'invalid-mail-message' },
          'Format podanego adresu email jest niepoprawny. Przykład poprawnego adresu to: nazwa@domena.pl'
        ) : null
      )
    );
  }
});

module.exports = Login;
