var React = require('react');

var Logo = React.createClass({
  render: function() {
    return React.DOM.div({ className: 'rb-logo' });
  }
});

module.exports = Logo;
