var React = require('react');
var classNames = require('classnames');
var screenRatioFactor = require('../../Config').screenRatioFactor;

var MicTestAnimation = React.createFactory(require('./mic-test-animation'));

var MicTest = React.createClass({
    getInitialState: function () {
        return {
            volume: 0,
            micTestDone: false
        };
    },

    componentDidMount: function () {
        this.props.backend.on('microphone', function (e) {
            this.setState({volume: e.volume});
        }.bind(this));

        this.props.backend.on('mictest:done', function () {
            this.setState({micTestDone: true});
        }.bind(this));

        this.props.backend.startMicTest();
    },

    onClickNext: function () {
        if (this.state.micTestDone) {
            this.props.stateMachine.transition('karaoke', this.props.data);
        }
    },

    render: function () {
        var volume = this.state.volume;

        var testInProgress = React.DOM.div(
            {className: 'mic-test-paragraph'},
            'Mów swoim naturalnym głosem, mikrofon dostosuje się automatycznie'
        );

        var testDone = React.DOM.div(
            {className: 'mic-test-paragraph'},
            'Mikrofon został ustawiony, możesz przejść dalej'
        );

        return React.DOM.div(
            {className: 'mic-test-container'},
            React.DOM.div(
                {className: 'mic-test-header'},
                'Przeczytaj "Test mikrofonu raz, dwa, trzy... Jestem w kabinie RecBox i szykuję się do nagrania."'
            ),
            this.state.micTestDone ? testDone : testInProgress,
            React.DOM.div(
                {
                    className: classNames('next-view-button', {inactive: !this.state.micTestDone}),
                    onClick: this.onClickNext
                },
                'Rozpocznij nagranie'
            ),
            MicTestAnimation({
                width: 1920 * screenRatioFactor,
                height: 1660 * screenRatioFactor,
                volume: volume
            }),
            React.DOM.img({className: 'mic-test-image', src: 'images/mic-test.svg'})
        );
    }
});

module.exports = MicTest;
