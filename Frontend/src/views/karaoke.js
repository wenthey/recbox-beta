var Config     = require('../../config');
var React      = require('react');
var extend     = require('extend');
var remap      = require('re-map');
var uuid       = require('uuid');
var fs         = require('fs');
var LRC        = require('../lrc-parser');

var CancelView = React.createFactory(require('./cancel'));
var Footer     = React.createFactory(require('./library/footer'));

var DEBUG = false;

var Karaoke = React.createClass({
  getInitialState: function() {

    // =====================================
    // debug - uncomment 
    // =====================================
    // =====================================

     //this.props.data = {
     //  email: "office@mgorski.com",
     //  song: {
     //    _id: "JDSitRyP85vKRvkI",
     //    artist: "Cyndi Lauper",
     //    audioFileURL: "",
     //    dateAdded: "2015-02-15",
     //    duration: "3:47",
     //    fullAudio: "15.wav",
     //    genre: {},
     //    id: 15,
     //    imageFullSizeURL: "15.jpg",
     //    imageThumbnailURL: "15.jpg",
     //    language: "angielski",
     //    lyricsURL: "15.lrc",
     //    parentalAdvisoryWarning: "",
     //    recCount: "0",
     //    title: "She-Bop"
     //  }
     //};

    // =====================================
    // =====================================

    // like 5:11 or 12:02
    var secondsFromDuration = function(durationString) {
      var timeSplit = durationString.replace(' (approx)', '').split(':')
      return parseInt(timeSplit[0], 10) * 60 + parseInt(timeSplit[1], 10);
    };

    return {
      finishSessionModal: false,
      progress:           0,
      recordPath:         Config.audioRecordingsFolder + uuid.v1() + '.wav',
      totalSeconds:       secondsFromDuration(this.props.data.song.duration),
      lyrics:             [],
      loaded_lyrics:      false,
      loaded_audio:       true
    };
  },

  componentWillMount: function() {
    this.readLRC();
  },
  componentDidMount: function() {
    this.props.backend.on('record:done', function() {
      var data = extend({}, this.props.data, { recordPath: this.state.recordPath });
      this.props.stateMachine.transition('choosen-song', data);
    }.bind(this));

    this.props.backend.on('progress', function(e) {
      if (this.isMounted()) {
        this.setState({ progress: e.progress });
      }
    }.bind(this));

    this.props.backend.recordSong(this.state.recordPath);
  },

  readLRC: function() {
    fs.readFile(Config.lyricsLibraryFolder + '/' + this.props.data.song.lyricsURL, { encoding: 'utf-8' }, function(error, data) {
      if (error) {
        console.log(error);
        return;
      }

      this.setState({lyrics: LRC(data), loaded_lyrics: true});

    }.bind(this));
  },

  onVolumeChange: function(v) {
    this.props.backend.setHeadphonesVolume(v);
  },

  onClickFinish: function() {
    this.setState({ finishSessionModal: true });
  },

  abortSession: function() {
    var data = extend({}, this.props.data, { recordPath: this.state.recordPath });
    this.props.stateMachine.transition('library-newest', data);
  },

  stopExiting: function() {
    this.setState({ finishSessionModal: false });
  },

  renderLyricsLines: function(secondsFromProgress, currentTimePlayed) {

    // new modifiers
    var numberOfLinesToShow = 5;

    // legacy - unused will be deleted in next itteration
    var screenHeight        = 1020 * Config.screenRatioFactor;
    var minFont             = 22 * Config.screenRatioFactor;
    var maxFont             = 48 * Config.screenRatioFactor;
    var maxColor            = 255;
    var minColor            = 72;
    var heightMod           = remap(secondsFromProgress, 0, this.state.totalSeconds, - screenHeight * 10, 0);


    var secondsFromString = function(string) {
      var splited = string.split(':').map(function(t) { return parseInt(t, 10); });
      return splited[0] * 60 + splited[1];
    };

    var nextLinesToShow = [];
    for (var k in this.state.lyrics) {

        // we want x lines
        if (nextLinesToShow.length >= numberOfLinesToShow) { break; }

        var d = this.state.lyrics[k];
        var showTime = secondsFromString(d.time.split('.').shift());

        /* collect lines TO SHOW */

        /*
          collect current line at first place 
          it's the last line which is 'outdated'
          currentTime is higher than their show time

          always put it at the beginnig of the list
        */  
        if (currentTimePlayed > showTime) {
          // additional class for current line
          this.state.lyrics[k].class = 'karaoke-line-current';
          nextLinesToShow[0] = this.state.lyrics[k];
        }

        /*
          next thing to collect is upcoming lines
          that are the lines with show time higher than currentTime

          append them to the end of list 
        */
        if (showTime > currentTimePlayed) {
          // additional class for next line
          this.state.lyrics[k].class = 'karaoke-line-next next-' + nextLinesToShow.length + ' ' + ( showTime - currentTimePlayed <= 1 ? ' next-in-less-than-sec' : '' );
          nextLinesToShow.push(this.state.lyrics[k]);
        }
    }

    return nextLinesToShow.map(function(d) {

      var time             = secondsFromString(d.time.split('.').shift());
      var line             = d.text;

      var position         = Math.round(remap(time, 0, this.state.totalSeconds, 0, screenHeight * 10) + heightMod);

      var distanceToCenter = Math.abs(position / screenHeight);
      distanceToCenter     = distanceToCenter >= 0 ? (1 - distanceToCenter) : (distanceToCenter - 1);

      var color            = Math.min(Math.round(distanceToCenter * (maxColor - minColor) + minColor), 255);
      var fontSize         = distanceToCenter * (maxFont - minFont) + minFont;

      return React.DOM.div(
          {
            className: d.class,
            // className: 'next-karaoke-line',
            // style: {
            //   transform: 'translateY(' + (screenHeight / 2 + position) + 'px )',
            //   color:     'rgb(' + color + ',' + color + ',' + color + ')',
            //   fontSize:  fontSize
            // }
          },
          DEBUG ?
          line + ' ( t:' + Math.round(currentTimePlayed) + '>' + time + ' c:' + d.class + ')' : 
          line
      );
    }.bind(this));
  },

  render: function() {
    // wait for text and audio
    if (false === this.state.loaded_lyrics && false === this.state.loaded_audio) {
      return false;
    }

    var stringFromSeconds = function(seconds) {
      var addPadding = function(seconds) {
        return (seconds < 10) ? ('0' + seconds) : seconds;
      };

      var displayMinutes = addPadding(Math.floor(seconds / 60));
      var displaySeconds = addPadding(Math.floor(seconds % 60));

      return displayMinutes + ':' + displaySeconds;
    };

    var secondsFromProgress = (1 - this.state.progress) * this.state.totalSeconds;
    var currentTimePlayed   = this.state.totalSeconds - secondsFromProgress;
    var secondsPlayedString = stringFromSeconds(currentTimePlayed);

    return React.DOM.div(
      { className: 'karaoke-container', style: { backgroundImage: 'url(\'' + Config.imagesFullFolder + '/' + this.props.data.song.imageFullSizeURL + '\')'} },
      React.DOM.div({ className: 'progress-line', style: { width: (this.state.progress * 100) + '%' } }, ''),
      React.DOM.div({ className: 'black-aple'}),
      this.state.finishSessionModal
          ? CancelView({ abortSession: this.abortSession, stopExiting: this.stopExiting })
          : null,
      React.DOM.div({ className: 'karaoke-text-container' }, this.renderLyricsLines(secondsFromProgress, currentTimePlayed)),
      Footer({  
        artist:         this.props.data.song.artist,
        title:          this.props.data.song.title,
        onVolumeChange: this.onVolumeChange
      }),
      React.DOM.div(
          { className: 'controls-container' },
          React.DOM.div({ className: 'rb-logo', style: {marginTop: '40px'} }), // single Logo implemetation to teh current view REMEMBER: to render that part in every view without menu
          React.DOM.div({ className: 'karaoke-btn', onClick: this.onClickFinish }, 'Zakończ'),
          React.DOM.div(
              { className: 'time' },
              React.DOM.img({ src: 'images/rec-indicator.svg'}),
              React.DOM.span({ className: 'left'  }, secondsPlayedString + ' / '),
              React.DOM.span({ className: 'total' }, stringFromSeconds(this.state.totalSeconds))
          )
        )
    );
  }
});

module.exports = Karaoke;
