var React          = require('react');
var times          = require('lodash.times');

var TransitionAnimation = React.createClass({
    animationHandle : undefined,
    lines: [
        {name: 'transitionLine3', amplitude: 250, width: 100, color: '#AAC9D7', speed: 80, stopHandler: undefined},
        {name: 'transitionLine1', amplitude: 350, width: 150, color: '#607D8B', speed: 120, stopHandler: undefined},
        {name: 'transitionLine2', amplitude: 450, width: 200, color: '#484848', speed: 160, stopHandler: undefined}
    ],
    componentDidMount: function() {
        this.animation();
    },
    componentWillUnmount: function() {
        this.animationHandle.stop();
    },
    animation: function() {
        var canvas = document.getElementById('transition_animation'),
            context = canvas.getContext('2d'),
            animationInterval,
            startY = canvas.height * 0.7;

        var animate = function(canvas, context, lines) {

            context.clearRect(0, 0, canvas.width, canvas.height);

            lines.forEach(function(line){
                if (undefined == line.stepProgress) {
                    line.stepProgress= -1;
                }

                line.stepProgress = (line.stepProgress + line.speed / 1000) % 2;

                var amplitudeSign = 1,
                    startX = line.width * line.stepProgress,
                    initialwidthMultiplier = -3,
                    widthMultiplier = initialwidthMultiplier;

                context.beginPath();

                do {
                    var multipliedWidth = line.width * widthMultiplier;

                    context.moveTo(startX + multipliedWidth, startY);

                    context.quadraticCurveTo(
                        startX + (line.width / 2) + multipliedWidth,
                        startY + (line.amplitude * amplitudeSign),
                        startX + (line.width) + multipliedWidth,
                        startY
                    );

                    // iterators
                    amplitudeSign = -1 * amplitudeSign;
                } while(line.width * (widthMultiplier++ + initialwidthMultiplier) <= canvas.width);

                context.lineWidth = 4;
                context.strokeStyle = line.color;
                context.stroke();
            });
        };

        animationInterval = setInterval(function(){
            animate(canvas, context, this.lines);
        }.bind(this), 48);

        this.animationHandle = {
            stop: function() {
                console.log('animation stopped');
                clearInterval(animationInterval);
            }
        };
    },

    render: function() {
        return React.DOM.div(
            { className: 'transition-animation' },
            React.DOM.canvas({
                id: 'transition_animation',
                width: window.innerWidth,
                height: window.innerHeight,
                style: {position: 'absolute', bottom: 50, left: 0}
            })
        );
    }
});

module.exports = TransitionAnimation;
