var React = require('react');

var CancelView = React.createClass({
  render: function() {
    return React.DOM.div(
      { className: 'cancel-view-container' },
      React.DOM.div({ className: 'main-header' }, 'Czy na pewno chcesz wyjść?'),
      React.DOM.div(
        { className: 'sub-header'},
        'Dotychczasowe nagranie przepadnie.'
      ),
      React.DOM.div(
        { className: 'buttons-container'},
        React.DOM.div({ className: 'abort-recording', onClick: this.props.abortSession }, 'Tak'),
        React.DOM.div({ className: 'send-recording', onClick: this.props.stopExiting }, 'Jeszcze nie')
      )
    );
  }
});

module.exports = CancelView;
