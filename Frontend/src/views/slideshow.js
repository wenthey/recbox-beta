var React = require('react');

var Slider = React.createFactory(require('react-slick'));

var Slideshow = React.createClass({
    getInitialState: function() {
        return (
            {sliderContent: ''}
        );
    },
    componentDidMount: function() {
        // @todo: there have to be better place for this line
        this.props.globalContainer.flushStorage();
    
        this.setState({sliderContent: this.sliderView()});
    },
    onClickStart: function () {
        this.props.stateMachine.transition('login');
    },
    sliderView: function () {
        return (
            Slider(
                {
                    arrows: false,
                    autoplay: true,
                    autoplaySpeed: 5000,
                    dots: false,
                    infinite: true,
                    speed: 2000,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    fade: true,
                    swipe: false
                },
                React.DOM.img({src: 'images/slide-1.png'}),
                React.DOM.img({src: 'images/slide-2.png'}),
                React.DOM.img({src: 'images/slide-3.png'}),
                React.DOM.img({src: 'images/slide-4.png'}),
                React.DOM.img({src: 'images/slide-5.png'})
            )
        );
    },
    render: function () {
        return React.DOM.div(
            {className: 'slideshow-container'},
            //this.sliderView(),
            React.DOM.div({ className: 'rb-logo', style: {marginTop: '10px'} }), // single Logo implemetation to teh current view REMEMBER: to render that part in every view without menu
            this.state.sliderContent,
            React.DOM.div(
                {className: 'elements-container'},
                React.DOM.h1({className: 'sing-with-us-header'}, 'Zaśpiewaj z nami'),
                React.DOM.h2(
                    {className: 'teaser-text'},
                    'Wybierz spośród wielu największych hitów muzyki światowej lub ',
                    'regionalnej. Ustaw podkład, przetestuj mikrofon i do dzieła. W kilka ',
                    'sekund przygotujemy dla Ciebie nagranie jakości studyjnej, które ',
                    'możesz przesłać znajomym, rodzinie lub zachować dla siebie.'
                ),
                React.DOM.div({className: 'start-button', onClick: this.onClickStart}, 'Zaczynamy')
            )
        );
    }
});

module.exports = Slideshow;
