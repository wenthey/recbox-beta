var Config = require('../config');
var async  = require('async');
var del    = require('del');
var extend = require('extend');
var fs     = require('fs');
var io     = require('socket.io-client');
var ss     = require('socket.io-stream');

function UploadManager() {
  this.queue  = async.queue(this.streamToServer.bind(this), 1);
  this.socket = io.connect(Config.serverUrl + '/upload');
}

/**
 * adds filePath to the queue and uploads it with additional metadata
 * callbacks when node, the file can be delated after callback
 * @param {string} filePath - absolute path for file to upload ("/Users/Audiobox/Temp/rec1.mp3")
 * @param {Object} metadata - additional file metadata
 * @param {callback} callback - callback after file is uploaded to server
 */
UploadManager.prototype.uploadFile = function(filePath, metadata, callback) {
  this.queue.push({ filePath: filePath, metadata: metadata }, function() {
    if (callback) { callback(); }
  });
};

UploadManager.prototype.streamToServer = function(task, callback) {
  var filePath = task.filePath;
  var data     = extend(task.metadata, { filePath: filePath });

  var stream = ss.createStream();

  stream.on('end', function() {
    del(filePath, callback);
  });

  ss(this.socket).emit('recording-upload', stream, data);

  fs.createReadStream(filePath).pipe(stream);
};

UploadManager.getInstance = function() {
  if (!this.instance) {
    this.instance = new UploadManager();
  }

  return this.instance;
};

module.exports = UploadManager.getInstance();
