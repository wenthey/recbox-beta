var io     = require('socket.io-client');
var Config = require('../config');

function DataCollector() {
  this.instance  = null;
  this.name      = null;
  this.collector = io.connect(Config.serverUrl + '/log');

  this.reset();
}

DataCollector.prototype.reset = function() {
  this.sessionStartTime = null;
  this.songsData        = { searched: [], selected: {} };
};

DataCollector.prototype.init = function(name, views) {
  this.name      = name;
  this.viewsData = views.reduce(function(memo, view) {
    memo[view.name] = view;
    return memo;
  }, {});
};

DataCollector.prototype.sessionStart = function() {
  this.sessionStartTime = new Date();
};

DataCollector.prototype.sessionEnd = function() {
  var sessionStartTime = this.sessionStartTime;

  var viewsData = Object.keys(this.viewsData)
    .map(function(key) {
      // turn object into array
      return this.viewsData[key];
    }.bind(this))
    .reduce(function(memo, viewData) {
      return viewData.viewTime ? memo.concat([ viewData ]) : memo;
    }, [])
    .sort(function(a, b) {
      return new Date(a.viewTime) - new Date(b.viewTime);
    })
    .map(function(viewData, index, array) {
      // calculate time in each view
      var timeInView;

      if (index > 0) {
        timeInView = viewData.viewTime - array[index - 1].viewTime;
      }
      else {
        timeInView = viewData.viewTime - sessionStartTime;
      }

      viewData.timeInView = timeInView;

      return viewData;
    })
    .map(function(viewData) {
      // remove unneded value
      delete viewData.viewTime;

      return viewData;
    });

  var sessionData = {
    name:      this.name,
    viewsData: viewsData,
    songsData: this.songsData,
    totalTime: new Date() - sessionStartTime,
    startTime: sessionStartTime
  };

  // send all collected data to server
  this.collector.emit('data:session-data', sessionData);
  // reset views data after sending to client
  this.reset();
};

DataCollector.prototype.timeInView = function(viewName) {
  var viewData = this.viewsData[viewName];

  if (viewData) {
    viewData.viewTime = new Date();
  }
};

// strings that user was looking for
DataCollector.prototype.searchForSong = function(songString) {
  this.songsData.searched.push(songString);
};

// actual song data
DataCollector.prototype.selectedSong = function(song) {
  this.songsData.selected = song;
};

DataCollector.getInstance = function() {
  if (!this.instance) {
    this.instance = new DataCollector();
  }

  return this.instance;
};

module.exports = DataCollector.getInstance();
