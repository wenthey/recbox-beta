/*global document */

var React = require('react/addons');
var StateMachine = require('./state-machine');
var dataCollector = require('./data-collector');
var Config = require('../config');

var ReactCSSTransitionGroup = React.createFactory(React.addons.CSSTransitionGroup);
var LiveReload = React.createFactory(require('./live-reload'));

var LibraryManager = React.createFactory(require('./views/library/library-manager'));
var ChoosenSong = React.createFactory(require('./views/choosen-song'));
var SongDetails = React.createFactory(require('./views/library/song-details'));
var SongSearch = React.createFactory(require('./views/library/song-search'));
var SearchResults = React.createFactory(require('./views/library/search-results'));
var Newest = React.createFactory(require('./views/library/newest'));
var Genres = React.createFactory(require('./views/library/genres'));

var Login = React.createFactory(require('./views/login'));
var MicTest = React.createFactory(require('./views/mic-test'));
var Karaoke = React.createFactory(require('./views/karaoke'));
var EndScreen = React.createFactory(require('./views/end-screen'));

var Logo = React.createFactory(require('./views/logo'));
var NoConnection = React.createFactory(require('./views/no-connection'));
var Slideshow = React.createFactory(require('./views/slideshow'));
var Transition = React.createFactory(require('./views/transition'));

var io = require('socket.io-client');
var socket = io.connect(Config.serverUrl + '/log');

var beckendMock = require('../config').backendMock;
var Backend = true === beckendMock ? require('./backend_mock') : require('./backend');
var backend = new Backend();

var AudioLibrary = require('./audio-library');
var audioLibrary = new AudioLibrary();
var Datastore = require('nedb');

var GlobalContainer = require('./GlobalContainer');

var Main = React.createClass({
    getInitialState: function () {
        return {
            currentState: Config.startView,
            audioLibrary: null
        };
    },

    componentWillMount: function () {
        socket.on('connect', function () {
            socket.emit('data:status', {name: Config.name});
        });

        socket.on('disconnect', function () {
            this.setState({currentState: 'no-connection'});
        }.bind(this));

        socket.on('reconnect', function () {
            socket.emit('data:status', {name: Config.name});
            this.setState({currentState: 'login'});
        }.bind(this));

        this.pingServerHandler = setInterval(function () {
            socket.emit('data:status', {name: Config.name});
        }.bind(this), Config.serverPingInterval);

        dataCollector.init(
            Config.name,
            [
                {name: 'login'},
                {name: 'song-details'},
                {name: 'song-search'},
                {name: 'search-results'},
                {name: 'library-newest'},
                {name: 'library-genres'},
                {name: 'mic-test'},
                {name: 'karaoke'},
                {name: 'choosen-song'},
                {name: 'end-screen'}
            ]
        );

        this.stateMachine = new StateMachine({
            initialState: this.state.currentState,
            states: [
                {
                    name: 'no-connection',
                    transitions: {
                        restart: 'slideshow'
                    }
                },
                {
                    name: 'slideshow',
                    transitions: {
                        login: 'login'
                    }
                },
                {
                    name: 'login',
                    transitions: {
                        'library-newest': 'library-newest'
                    }
                },
                {
                    name: 'song-details',
                    transitions: {
                        sing: 'mic-test',
                        genres: 'library-genres',
                        newest: 'library-newest',
                        'song-search': 'song-search',
                        quit: 'slideshow'
                    }
                },
                {
                    name: 'song-search',
                    transitions: {
                        'search-results': 'search-results',
                        'song-search': 'song-search',
                        genres: 'library-genres',
                        newest: 'library-newest',
                        quit: 'slideshow'
                    }
                },
                {
                    name: 'search-results',
                    transitions: {
                        'song-details': 'song-details',
                        'song-search': 'song-search',
                        genres: 'library-genres',
                        newest: 'library-newest',
                        quit: 'slideshow'
                    }
                },
                {
                    name: 'library-newest',
                    transitions: {
                        'song-details': 'song-details',
                        'song-search': 'song-search',
                        genres: 'library-genres',
                        genre: 'search-results',
                        newest: 'library-newest',
                        quit: 'slideshow'
                    }
                },
                {
                    name: 'library-genres',
                    transitions: {
                        'song-details': 'song-details',
                        'song-search': 'song-search',
                        genres: 'library-genres',
                        'search-results': 'search-results',
                        newest: 'library-newest',
                        quit: 'slideshow'
                    }
                },
                {
                    name: 'mic-test',
                    transitions: {
                        karaoke: 'karaoke'
                    }
                },
                {
                    name: 'karaoke',
                    transitions: {
                        'library-newest': 'library-newest',
                        'choosen-song': 'choosen-song'
                    }
                },
                {
                    name: 'choosen-song',
                    transitions: {
                        'end-screen': 'end-screen',
                        restart: 'slideshow'
                    }
                },
                {
                    name: 'end-screen',
                    transitions: {
                        login: 'login',
                        timeout: 'slideshow'
                    }
                }
            ]
        });

        this.stateMachine.on('transition', function (transitionObject) {
            dataCollector.timeInView(transitionObject.from);

            this.setState({transitionData: transitionObject.data});

            var shouldDisplayTransition = [
                {to: 'slideshow'},
                {to: 'login'},
                {to: 'library-newest', from: 'login'},
                {to: 'mic-test'},
                {to: 'karaoke'},
                {to: 'choosen-song'},
                {to: 'end-screen'}
            ].some(function (transitionViewName) {
                    var fromStateCorrect = !transitionViewName.from || transitionViewName.from === transitionObject.from;
                    var toStateCorrect = !transitionViewName.to || transitionViewName.to === transitionObject.to;

                    return toStateCorrect && fromStateCorrect;
                });

            if (shouldDisplayTransition) {
                this.setState({currentState: 'transition'});

                setTimeout(function () {
                    this.setState({currentState: transitionObject.to});
                }.bind(this), Config.transitionTime);
            }
            else {
                this.setState({currentState: transitionObject.to});
            }
        }.bind(this));

        //audioLibrary.generate(function() {
        //  this.setState({ audioLibrary: audioLibrary });
        //}.bind(this));
    },

    componentWillUnmount: function () {
        clearInterval(this.pingServerHandler);
    },

    render: function () {
        var Views = {
            'no-connection': NoConnection,
            slideshow: Slideshow,
            transition: Transition,
            login: Login,
            'song-details': LibraryManager,
            'song-search': LibraryManager,
            'search-results': LibraryManager,
            'library-newest': LibraryManager,
            'library-genres': LibraryManager,
            'mic-test': MicTest,
            karaoke: Karaoke,
            'choosen-song': ChoosenSong,
            'end-screen': EndScreen
        };

        // @todo: po co to?
        //var shouldLoad = this.state.audioLibrary !== null;
        var shouldLoad = true;

        var db = {
            songs: new Datastore({filename: './db/songs.db', autoload: true}),
            genres: new Datastore({filename: './db/genres.db', autoload: true})
        };

        return React.DOM.div(
            {className: 'main'},
            LiveReload(),
            //Logo(),
            ReactCSSTransitionGroup(
                {transitionName: 'fade'},
                shouldLoad ? Views[this.state.currentState]({
                    key: this.state.currentState,
                    stateMachine: this.stateMachine,
                    backend: backend,
                    data: this.state.transitionData,
                    currentState: this.state.currentState,
                    audioLibrary: audioLibrary,
                    db: db,
                    globalContainer: GlobalContainer
                }) : null
            )
        );
    }
});

React.render(React.createElement(Main), document.body);
