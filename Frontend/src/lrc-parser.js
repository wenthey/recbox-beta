module.exports = function(string) {
  return string.split('\n').reduce(function(memo, line) {
    var numRegex = new RegExp('^\\[[0-9][0-9]:[0-9][0-9].[0-9][0-9]\\]');

    if (numRegex.test(line)) {
      var time = line.split(']')[0].replace('[', '');
      var text = line.split(']')[1];

      var timeSplit = time.replace(' (approx)', '').split('.');
      var timeMinutesAndSeconds = timeSplit[0].split(':');
      var seconds = parseInt(timeMinutesAndSeconds[0], 10) * 60 + parseInt(timeMinutesAndSeconds[1], 10);

      memo.push({ time: time, text: text, seconds: seconds, h_seconds: parseInt(timeSplit[1]) });
    }

    return memo;
  }, []);
};
