var EventEmitter = require('events').EventEmitter;
var util         = require('util');

function StateMachine(config) {
  var isInitialStateValid = function(config) {
    return config.states.some(function(transition) {
      return transition.name === config.initialState;
    });
  };

  if (!isInitialStateValid(config)) {
    throw new Error('Provided initialState does not exist in any \'name\' field in states array.');
  }

  this.states       = config.states;
  this.currentState = config.initialState;
}

util.inherits(StateMachine, EventEmitter);

StateMachine.prototype.isTransitionAllowed = function(transitionName) {
  var toState = this.getTransitionByName(transitionName);
  return (toState !== null && toState !== undefined) ? true : false;
};

StateMachine.prototype.getCurrentStateObject = function() {
  return this.states.filter(function(transition) {
    if (transition.name === this.currentState) {
      return transition;
    }
  }.bind(this))[0];
};

StateMachine.prototype.getTransitionByName = function(transitionName) {
  var currentStateObject = this.getCurrentStateObject(transitionName);
  return currentStateObject.transitions[transitionName];
};

StateMachine.prototype.transition = function(transitionName, transitionData) {
  if (this.isTransitionAllowed(transitionName)) {
    var toState = this.getTransitionByName(transitionName);

    var transitionObject = { from: this.currentState, to: toState, data: transitionData || {} };
    this.currentState    = toState;

    this.emit('transition', transitionObject);
  }
  else {
    var errorString = 'Transition not allowed. No \'';
    errorString += transitionName;
    errorString += '\' event exists for \'';
    errorString += this.currentState;
    errorString +=  '\' state.';

    throw new Error(errorString);
  }
};

module.exports = StateMachine;
