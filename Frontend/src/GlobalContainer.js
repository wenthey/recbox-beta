// Global container (pseudo Session)
// ------------------------------
var GlobalContainer = {
    storage: {},
    save (values) {
        if ('Array' === typeof values) { console.error('Values provided to GlobalContainer.save should be in Array') }

        for (var k in values) {
            this.storage[k] = values[k];
        }
    },
    get (key) {
        return key in this.storage ? this.storage[key] : undefined;
    },
    flushStorage () {
        this.storage = {};
    }
};

module.exports = GlobalContainer;