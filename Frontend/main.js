/*jslint unparam: true */

var app           = require('app');
var BrowserWindow = require('browser-window');
var Menu          = require('menu');
var Config        = require('./config');

var mainWindow    = null;

app.on('ready', function() {
  var menu = Menu.buildFromTemplate([
    {
      label: 'Electron',
      submenu: [
        {
          label:       'Quit',
          accelerator: 'CommandOrControl+Q',
          click:       function() { app.quit(); }
        }
      ]
    },
    {
      label: 'Develop',
      submenu: [
        {
          label:       'Toggle DevTools',
          accelerator: 'Alt+CommandOrControl+C',
          click:       function(item, window) { window.toggleDevTools(); }
        }
      ]
    }
  ]);

  app.setApplicationMenu(menu);

  var width  = (1920 * Config.screenRatioFactor + Config.devToolsWidth);
  var height = (1080 * Config.screenRatioFactor);

  mainWindow = new BrowserWindow({
    width:           width,
    height:          height,
    kiosk:           Config.fullscreen,
    fullscreen:      false,
    resizable:       !Config.fullscreen,
    frame:           !Config.fullscreen,
    'always-on-top': Config.fullscreen,
    'experimental-features': true,
    'experimental-canvas-features': true
  });

  mainWindow.loadUrl('file://' + __dirname + '/public/index.html');
  mainWindow.on('closed', function() { mainWindow = null; });

  // FIXME: somehow this brakes the touch screen when run in production, need to investigate
  // if (!Config.isProduction) {
    mainWindow.openDevTools();
  // }
});

app.on('window-all-closed', function() {
  app.quit();
});

// experimental javascript live-reload

var chalk      = require('chalk');
var dateformat = require('dateformat');
var watch      = require('watch');
var less       = require('less');

var logger = function(string) {
  var date = '[' + chalk.cyan(dateformat(new Date(), 'HH:MM:ss')) + ']';
  console.log(date + ' ' + string);
};

watch.watchTree('./src/', function() {
  logger('src change detected!');
  mainWindow.loadUrl('file://' + __dirname + '/public/index.html');
});

// experimental live-less compilation

var io = require('socket.io')(3002);
var fs = require('fs');

watch.watchTree('./less/', function() {
  logger('less change detected!');

  var lessStylePath = __dirname + '/less/style.less';
  var cssStylePath  = __dirname + '/public/style.css';

  var saveFile = function(path, data, callback) {
    fs.writeFile(path, data, function(err) {
      if (err) {
        console.log(err);
      }
      else {
        callback();
      }
    });
  };

  var options = {
    modifyVars: {
      'screen-ratio-factor': Config.screenRatioFactor
    }
  };

  var lessRender = function(outputPath, data) {
    less.render(
      data,
      options,
      function(err, compiled) {
        if (err) {
          console.log(err);
        }
        else {
          saveFile(outputPath, compiled.css, function() {
            io.emit('live-css-reload:ready');
          });
        }
      }
    );
  };

  fs.readFile(lessStylePath, 'utf8', function(err, data) {
    if (err) {
      console.log(err);
    }
    else {
      lessRender(cssStylePath, data);
    }
  });
});
