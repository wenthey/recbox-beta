var initialGain = 50.0; // 0 - 65 //40
var gain        = initialGain;
var gainStep    = 1.0;
var gainMin     = 0.0;
var gainMax     = 65.0;
var targetVol   = -15.0; //12 was -10
var tolerance   = 1.0;

function msg_float(v) {
	if (Math.abs(v - targetVol) > tolerance) {
		if (v < targetVol) {
			gain += gainStep;
		}
		else if (v > targetVol) {
			gain -= gainStep;
		}

		gain = Math.max(gainMin, Math.min(gainMax, gain));
	}

	outlet(0, gain);
}

function reset() {
	gain = initialGain;
	outlet(0, gain);
}

function target(t) {
	targetVol = t;
}
