var gain      = 0.0;
var targetVol = -12.0;
var step      = 0.1;
var tolerance = 1.0;

function msg_float(v) {
	if (Math.abs(v - targetVol) > tolerance) {
		if (v < targetVol) {
			gain += step;
		}
		else if (v > target) {
			gain -= step;
		}
	}

	outlet(0, gain);
}

function reset() {
	gain = 0.0;
	outlet(0, gain);
}

function target(t) {
	targetVol = t;
}
