var SerialPort = require('serialport').SerialPort;
var async      = require('async');

function SerialWrite(port) {
  this.serialPort = new SerialPort(port, { baudrate: 9600 });

  this.serialPort.on('data', function(data) {
    console.log('data', data.toString());
  });
}

SerialWrite.prototype.open = function(callback) {
  this.serialPort.open(callback);
};

SerialWrite.prototype.close = function() {
  this.serialPort.close();
};

SerialWrite.prototype.writeBytes = function(string, callback) {
  var data = (string + '\r').split('');

  var writeAndDrain = function(data, callback) {
    this.serialPort.write(data, function(error, results) {
      this.serialPort.drain(callback);
    }.bind(this));
  }.bind(this);

  async.mapSeries(
    data,
    function(value, callback) {
      setTimeout(function() {
        writeAndDrain(value, callback);
      }, 10);
    },
    function() {
      if (callback) { callback(); }
    }
  );
};

module.exports = SerialWrite;
