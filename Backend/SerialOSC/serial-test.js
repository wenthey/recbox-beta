var UdpReceiver = require('omgosc').UdpReceiver;
var throttle    = require('lodash.throttle');

var udpPort     = 3005;
var udpReceiver = new UdpReceiver(udpPort);

var previousGain = -1;

setTimeout(function() {
  udpReceiver.on('/gain', throttle(function(e) {
    var gain = e.params[0];
    if (gain === previousGain) { return; }

    console.log('setting gain to: ' + gain);

    previousGain = gain;
  }, 500, { trailing: true }));
}, 1000);
