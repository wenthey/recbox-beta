var UdpReceiver = require('omgosc').UdpReceiver;
var throttle    = require('lodash.throttle');

var SerialWrite = require('./serial-write');
// var serialWrite = new SerialWrite('/dev/tty.usbserial-AI028M2W');
var serialWrite = new SerialWrite('/dev/tty.usbserial-A703TO55');

var udpPort     = 3005;
var udpReceiver = new UdpReceiver(udpPort);

serialWrite.open(function(error) {
  if (error) {
    console.log(error);
  }
  else {
    // turn phantom on
    serialWrite.writeBytes('P1');

    var previousGain = -1;

    setTimeout(function() {
      udpReceiver.on('/gain', throttle(function(e) {
        var gain = e.params[0];
        if (gain === previousGain) { return; }

        console.log('setting gain to: ' + gain);

        serialWrite.writeBytes('G' + gain);

        previousGain = gain;
      }, 100, { trailing: true }));
    }, 1000);
  }
});

process.on('exit', function() {
  console.log('exiting...');
  serialPort.close();
});
