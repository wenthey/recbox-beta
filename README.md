# Audiobox Repo

## Commits style:

```
  [Backend/Frontend/Server] commit_text
```

Example:

```
  [Server] added database API endpoints
  [Frontend] UI fix for text display in info view
  ...
```

## Branches style:

```
  [dev/fix]/feature_name
```

Example:

```
  dev/database-switch
  fix/login-problems
  ...
```

Working and tested branches are merged into master and put on git.

## OSC routes:

* `-> /progress` - recording progress
* `-> /microphone` - mic volume in mic test
* `-> /mictest-done` - mic test complete
* `-> /record-done` - recording done and file saved on disk
* `<- /volume` - headphones volume (playalong track gain, 0.0 - 1.0)
* `<- /load-song` - load playlong track (track as argument)
* `<- /mictest-start` - mic test start
* `<- /record-start` - recording start (track as argument)
* `<- /replay-start` - play recorded tract start
* `<- /replay-stop` - play recorded tract stop

## Requirements:

### Frontend:

* `node` (`brew install node`)
* `exiftool` (`brew install exiftool`)

### Server:

* `node`
* `mongodb`

### Backend:

* `Max/MSP`
* audio plugins
