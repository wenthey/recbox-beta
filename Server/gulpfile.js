var autoprefixer = require('gulp-autoprefixer');
var browserify   = require('browserify');
var browserSync  = require('browser-sync');
var buffer       = require('vinyl-buffer');
var chalk        = require('chalk');
var chmod        = require('gulp-chmod');
var fs           = require('fs');
var gulp         = require('gulp');
var gulpif       = require('gulp-if');
var jscs         = require('gulp-jscs');
var jslint       = require('gulp-jslint');
var less         = require('gulp-less');
var log          = require('gulp-util').log;
var minifycss    = require('gulp-minify-css');
var nodemon      = require('nodemon');
var source       = require('vinyl-source-stream');
var spawn        = require('child_process').spawn;
var uglify       = require('gulp-uglify');
var watch        = require('gulp-watch');
var watchify     = require('watchify');

var lintFiles = [ '*.js', 'src/app.js', 'src/node_modules/**/*.js', 'utils/**/*.js' ];
var devEnv;

try {
  devEnv = require('./env.local.js');
}
catch (ignore) {}

var error = function(error) {
  log(chalk.red(error.message));
  this.emit('end');
};

var shell = function(command, name) {
  var cmd  = command.split(' ').shift();
  var args = command.split(' ').slice(1);

  name = name || cmd;

  var spawned = spawn(cmd, args);

  spawned.stdout.on('data', function(data) {
    data
      .toString()
      .split('\n')
      .map(function(line)     { return line.replace(/^\.*\]/, '').trim(); })
      .filter(function(line)  { return line.length > 1; })
      .forEach(function(line) { log(chalk.cyan(name) + ' ' + line); });
  });

  spawned.on('exit', function() {
    this.emit('done');
  });

  return spawned;
};

var bundle = function(options) {
  var runWatchify = options ? options.watchify : false;
  var filePath    = './src/node_modules/client/app.js';
  var fileDest    = './src/public/client/';

  var bundler, envify;

  if (runWatchify) {
    bundler = watchify(browserify(filePath, watchify.args));
  }
  else {
    bundler = browserify(filePath);
  }

  if (process.env.NODE_ENV === 'production') {
    envify = require('envify');

    bundler.transform(envify);
  }
  else {
    envify = require('envify/custom');

    bundler.transform(envify(devEnv));
  }

  bundler.on('log', function(data) {
    var logString = data.split(' ').map(function(word) {
      word = word.replace(/\(|\)/g, '');
      return !isNaN(word) ? chalk.magenta(word) : word;
    }).join(' ');

    log(chalk.cyan('browserify') + ' ' + logString);
  });

  var rebundle = function() {
    if (process.env.NODE_ENV === 'production') {
      log(chalk.cyan('browserify') + ' running with uglify');
    }

    return bundler
      .bundle()
      .on('error', error)
      .pipe(source('app.js'))
      .pipe(gulpif(process.env.NODE_ENV === 'production', buffer()))
      .pipe(gulpif(process.env.NODE_ENV === 'production', uglify()))
      .pipe(chmod(644))
      .pipe(gulp.dest(fileDest));
  };

  if (runWatchify) {
    bundler.on('update', rebundle);
  }

  return rebundle();
};

var compile = function() {
  var filePath = './src/public/client/less/style.less';
  var fileDest = './src/public/client/';
  var lesser = less({ paths: [ __dirname + '/node_modules' ] }).on('error', error);

  return gulp.src(filePath)
    .pipe(lesser)
    .pipe(autoprefixer({ browsers: [ 'last 2 versions' ], cascade: false, remove: true }))
    .pipe(minifycss())
    .pipe(gulp.dest(fileDest));
};

var lint = function() {
  return gulp.src(lintFiles)
    .pipe(jslint({
      errorsOnly: true,
      newcap:     true,
      node:       true,
      nomen:      true,
      regexp:     true,
      sloppy:     true,
      todo:       true,
      vars:       true,
      white:      true
    }).on('error', error))
    .pipe(jscs().on('error', error));
};

gulp.task('browsersync', function() {
  browserSync({
    proxy:  'http://localhost:3001',
    files:  [ 'src/public/*/*.js', 'src/public/*/*.css' ],
    port:   3000,
    open:   false,
    ui:     false,
    minify: false
  });
});

gulp.task('nodemon', function() {
  var node = nodemon({
    script: 'app.js',
    watch: [
      'src/app.js',
      'src/node_modules/server/**/*',
      'src/node_modules/settings/**/*',
      'src/node_modules/shared/**/*'
    ],
    env: devEnv
  });

  node.on('log', function(data) {
    var message = data.type === 'fail' ? chalk.red(data.message) : data.message;
    log(chalk.cyan('nodemon') + ' ' + message);
  });
});

gulp.task('watch', function() {
  [
    { glob: 'src/public/**/*.less', task: 'less' },
    { glob: lintFiles,              task: 'lint' }
  ].forEach(function(object) {
    watch(object.glob, { name: object.task }, function() { gulp.start(object.task); });
  });
});

gulp.task('browserify', function() { return bundle(); });
gulp.task('watchify',   function() { return bundle({ watchify: true }); });
gulp.task('less',       function() { return compile(); });
gulp.task('lint',       function() { return lint(); });
gulp.task('dirs',       function() { return shell('mkdir ./temp/'); });
gulp.task('mongod',     function() { return shell('mongod --quiet --dbpath=/usr/local/var/mongodb'); });

gulp.task('run',        [ 'dirs', 'nodemon', 'watchify', 'mongod', 'browsersync', 'less', 'lint', 'watch' ]);
gulp.task('build',      [ 'dirs', 'browserify', 'less' ]);

gulp.task('default',    [ 'run' ]);
