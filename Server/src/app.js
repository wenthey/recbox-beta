var express  = require('express');
var extend   = require('extend');
var knox     = require('knox');
var mongojs  = require('mongojs');
var winston  = require('winston');

var settings = require('settings/server');
var mongodb  = mongojs(settings.db.url).on('error', console.log);

// Express

var app    = express();
var server = app.listen(settings.port);

if (settings.isProduction) {
  var compression = require('compression');
  var logger      = require('morgan');

  app.use(compression());
  app.use(logger('combined'));
}
else {
  // CORS for BrowserSync
  var cors = require('cors');

  app.use(cors({
    origin:      'http://localhost:3000',
    credentials: true
  }));
}

app.use('/public/', express.static(settings.dirs.client));

// Modules

var params = {
  app:    app,
  server: server,
  db:     mongodb
};

[ 'api', 'socket', 'login' ].forEach(function(module) {
  require('server/' + module)(params);
});

winston.info('app started on port: ' + settings.port);
