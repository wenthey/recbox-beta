var settings = require(__dirname + '/../src/node_modules/settings/server');
var bcrypt   = require('bcrypt-nodejs');
var mongojs  = require('mongojs');
var mongodb  = mongojs(settings.db.url);
var admins   = mongodb.collection(settings.db.collections.admins);

var password = process.argv[2];

if (!password) {
  console.log('put new admin password as argument');
  process.exit(1);
}

bcrypt.genSalt(8, function(error, salt) {
  if (error) {
    console.error(error);
  }
  else {
    bcrypt.hash(password, salt, null, function(error, password) {
      if (error) {
        console.error(error);
      }
      else {
        admins.drop(function() {
          admins.insert({ username: 'admin', password: password }, function() {
            console.log('updated!');
            process.exit(0);
          });
        });
      }
    });
  }
});
