#!/usr/bin/env bash

sudo apt-get update
sudo apt-get -y upgrade

# install needed packages
sudo apt-get install -y nodejs npm git mongodb

# node, not nodejs
sudo update-alternatives --install /usr/bin/node node /usr/bin/nodejs 10

# route 80 to 3000 for app
sudo iptables -t nat -nvL
sudo iptables -t nat -F
sudo iptables -t nat -A PREROUTING -p tcp --dport 80 -j REDIRECT --to-ports 3000

# forever for app restarts
sudo npm install -g forever

